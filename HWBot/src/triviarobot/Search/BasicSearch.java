/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import triviarobot.Logger.Channel;
import triviarobot.MiscTools.WebUtilities;
import triviarobot.Rtn.BasicRtn;
import static triviarobot.TriviaRobot.L;

/**
 *
 * @author Ol'
 * 
 */
public class BasicSearch extends SearchType{
    int depth = 0;
    public BasicSearch(String query ,String URL)
    {
        super(URL, query);
    }
    public BasicSearch(String query, String URL, int depth)
    {
        super(URL, query);
        this.depth = depth;
    }
    public BasicSearch(String query)
    {
        super(query);
    }
    public BasicSearch(String query, int depth)
    {
        super(query);
        this.depth = depth;
    }
    
    @Override
    public BasicRtn Search()
    {
        L.Log(Channel.ALGORITHMS_MONITOR_BASIC_SEARCH, "Starting Search");
        
        //Declarations  
        String featured = "";
        ArrayList<String> URLs = new ArrayList<>();
        ArrayList<String> textSummary = new ArrayList<>();        
        
        L.Log(Channel.ALGORITHMS_MONITOR_BASIC_SEARCH, "Fetching Document");
        try {
            results = Jsoup.connect(URL).userAgent(WebUtilities.USER_AGENT).timeout(100000).get();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(BasicSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        L.Log(Channel.ALGORITHMS_MONITOR_BASIC_SEARCH, "Grabbing Data");        
        featured = results.select(".Z0LcW").stream().map((result) -> result.text()).reduce(featured, String::concat);        
        results.select("h3.r a, span.st, span.st + div").stream().forEach((result) -> {
            if(result.attr("href").contains("http")) { URLs.add(result.attr("href")); }
            else
            {
                textSummary.add(result.text()); 
            }
        });
        
        
        L.Log(Channel.ALGORITHMS_MONITOR_BASIC_SEARCH, "Starting Sub Searches");
        ArrayList<CompletableFuture<Document>> extraSearches = new ArrayList<>();
        for(int j = 0; j < depth; j++)
        {
            if(j >= URLs.size())
                break;
            final String currURL = URLs.get(j);
            extraSearches.add(CompletableFuture.supplyAsync(() -> jsoupWrapper(currURL)));
        }
        for(int j = 0; j < extraSearches.size(); j++)
        {
            try {
                Document doc = extraSearches.get(j).get();
                if(doc != null && doc.body() != null && doc.body().text() != null)
                {
                    String text = doc.body().text();
                    textSummary.add(text);
                }
            } catch (InterruptedException | ExecutionException ex) {
                java.util.logging.Logger.getLogger(BasicSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        L.Log(Channel.ALGORITHMS_MONITOR_BASIC_SEARCH, "Summary: \n \tURL: " + URL + "\n\tQuery: " + query + "\n\tDocument: " + results.title());
        
        return (new BasicRtn(featured,URLs.toArray(new String[URLs.size()]),textSummary.toArray(new String[textSummary.size()]), query));
    }
    
    private Document jsoupWrapper(String url)
    {
        try {
            return Jsoup.connect(url).userAgent(WebUtilities.USER_AGENT).timeout(100000).get();
        } catch (IOException ex) {
           // java.util.logging.Logger.getLogger(BasicSearch.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
