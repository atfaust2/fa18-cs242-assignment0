/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Search;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import triviarobot.Logger.Channel;
import triviarobot.Rtn.BasicRtn;
import triviarobot.Rtn.WhichOfTheseRtn;
import static triviarobot.TriviaRobot.L;

/**
 *
 * @author Ol'
 */
public class WhichOfTheseSearch extends SearchType {
    
    public String [] choices;
    public WhichOfTheseSearch(String query, String[] choices)
    {
        super(query);
        this.choices = choices;
    }
    
    public WhichOfTheseSearch(String query, String[] choices, String URL)
    {
        super(URL, query);
        this.choices = choices;
    }
    
    
    @Override
    public WhichOfTheseRtn Search()
    {
        L.Log(Channel.ALGORITHMS_MONITOR_WOT, "Starting Search");
        BasicSearch [] queryList = new BasicSearch[choices.length];
        ArrayList<CompletableFuture<BasicRtn>> futures = new ArrayList<>();
        BasicRtn [] rtnList = new BasicRtn[choices.length];
        
        
        for(int i = 0; i < queryList.length; i++)
        {
            final BasicSearch s = new BasicSearch(choices[i], 2);
            futures.add(CompletableFuture.supplyAsync(() -> s.Search()));
        }
        for(int i = 0; i < futures.size(); i++)
        {
            try {
                rtnList[i] = futures.get(i).get();
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(WhichOfTheseSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        return new WhichOfTheseRtn(query, rtnList);
    }
    
}
