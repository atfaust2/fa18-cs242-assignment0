/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Search;


import org.jsoup.nodes.Document;
import triviarobot.Rtn.RtnType;
import triviarobot.MiscTools.WebUtilities;

/**
 *
 * @author Ol'
 */
public abstract class SearchType {
    String URL = WebUtilities.GOOGLE_URL;
    final String query;
    Document results;
    
    public SearchType(String query)
    {
        this.query = query;
        this.URL = WebUtilities.GOOGLE_URL + query;
    }
    
    public SearchType(String URL, String query)
    {
        this.URL = URL + query;
        this.query = query;
    }        
    
    public abstract RtnType Search();
}
