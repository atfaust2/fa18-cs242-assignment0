/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot;

import java.util.Arrays;
import triviarobot.Algorithm.BasicAlgorithm;
import triviarobot.Algorithm.BasicAlgorithmConcurrent;
import triviarobot.Algorithm.WhichOfTheseAlgorithm;
import triviarobot.Algorithm.WhichOfTheseAlgorithmConcurrent;
import static triviarobot.TriviaRobot.L;
/**
 *
 * @author Ol'
 */
public class TestRobot {
    private int gameSize;
    //First index is the question number, the second is in the order Question,Answer1,Answer2,Answer3,CorrectAnswer
    private String [] [] game;
    boolean useBasic = true;
    boolean useWOT = false;
    
    public TestRobot(String[][] game, int gameSize)
    {
        this.gameSize = gameSize;
        this.game = game;
    }
    
    public void run() throws Exception
    {
        int correct = 0;
        int idk = 0;
        int incorrect = 0;
        L.blockAll();
        L.unBlock(Logger.Channel.THROW_AWAY);
        for(int i = 0; i < gameSize; i ++)
        {
            System.out.println(game[i][0]);
            int [] predictions = new int[2];
            if(useBasic)
            {
                // CHANGE BACK !!!
                BasicAlgorithm  b = new BasicAlgorithm(Arrays.copyOfRange(game[i], 0, 4), 0);
//                WhichOfTheseAlgorithm  b = new WhichOfTheseAlgorithm(Arrays.copyOfRange(game[i], 0, 4));
                predictions[0] = b.getAnswer();
            }
            L.print();
            if(predictions[0] == 0 && useWOT && (" " + game[i][0].toLowerCase()).contains(" which "))
            {
                //MANUALLY INSERTED THRESHOLD FOR NOW
                WhichOfTheseAlgorithm w = new WhichOfTheseAlgorithm(Arrays.copyOfRange(game[i], 0 , 4), 5);
                predictions[1] = w.getAnswer();
            }
            //For now we just look for the first algorithm with an answer and answer that
            System.out.println(predictions[0]);    
            for(int j = 0; j < predictions.length; j++)
            {
                if(predictions[j] != 0)
                {
                    if((predictions[j]+"").equals(game[i][4]))
                    {
                        L.Log(Logger.Channel.THROW_AWAY, "Correct");
                        correct++;
                        break;
                    }
                    else
                    {
                        L.Log(Logger.Channel.THROW_AWAY, "Incorrect");
                        incorrect++;
                        break;
                    }
                }
                else
                {
                    if(j == predictions.length - 1)
                    {
                        L.Log(Logger.Channel.THROW_AWAY, "IDK");
                        idk++;
                    }
                }
            }
           System.out.println("Correct: " + (correct/(double)(correct + idk + incorrect)));
           System.out.println("IDK: " + (idk/(double)(correct + idk + incorrect)));
           System.out.println("Incorrect: " + (incorrect/(double)(correct + idk + incorrect)));
        }
    }
    
    public void runConcurrent(int batchSize) throws Exception
    {
        int correct = 0;
        int idk = 0;
        int incorrect = 0;
        for(int i = 0; i < gameSize/batchSize; i ++)
        {
            int [] idkIndices = new int[20];
            int k = 0;
            String [][] qList = new String[batchSize][4];
            if(useBasic)
            {
                for(int j = 0; j < batchSize; j++)
                    qList[j] = Arrays.copyOfRange(game[i*batchSize + j], 0, 4);
                BasicAlgorithmConcurrent b = new BasicAlgorithmConcurrent(qList, 5);
                int [] prediction = b.getAnswers();
                for(int j = 0; j < batchSize; j++)
                {
                    if((""+prediction[j]).equals(game[i*batchSize+j][4]))
                    {
                        L.Log(Logger.Channel.THROW_AWAY, "Correct");
                        correct++;      
                    }
                    else if (prediction[j] == 0)
                    {
                        if(useWOT)
                        {
                            idkIndices[k] = j;
                            k++;
                        }
                        else
                        {
                            L.Log(Logger.Channel.THROW_AWAY, "IDK");
                            idk++;
                        }
                    }
                    else
                    {             
                        L.Log(Logger.Channel.THROW_AWAY, "Incorrect");
                        incorrect++;
                    }
                    if(useWOT && k == batchSize - 1)
                    {
                        k = 0;
                        for(int l = 0;l < 20;l++)
                            qList[l] = Arrays.copyOfRange(game[idkIndices[l]], 0, 4);
                        WhichOfTheseAlgorithmConcurrent w = new WhichOfTheseAlgorithmConcurrent(qList, 5);
                        int [] WOTprediction = w.getAnswers();
                        for(int l = 0; l < batchSize; l++)
                        {
                            if((""+WOTprediction[l]).equals(game[idkIndices[l]][4]))
                                correct++;
                            else if(WOTprediction[l] == 0)
                                idk++;
                            else
                            {
                                incorrect++;
                            }
                        }
                    }
                }
                L.print();
           //     System.out.println("Correct: " + (correct/(double)(correct + idk + incorrect)));
           //     System.out.println("IDK: " + (idk/(double)(correct + idk + incorrect)));
           //     System.out.println("Incorrect: " + (incorrect/(double)(correct + idk + incorrect)));
            }
        }   
    }
}
