/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Processor;

import triviarobot.MiscTools.Utilities;
import triviarobot.Rtn.BasicRtn;
import triviarobot.Rtn.WhichOfTheseRtn;

/**
 *
 * @author Ol'
 */
public class WhichOfTheseProcessor extends ProcessorType{
    
    double [] freqArray;
    double threshold = 0;
    public WhichOfTheseProcessor(WhichOfTheseRtn info, String [] answerChoices)
    {
         super(info, answerChoices);
         freqArray = new double[answerChoices.length];
    }
    public WhichOfTheseProcessor(WhichOfTheseRtn info, String [] answerChoices, double threshold)
    {
         super(info, answerChoices);
         freqArray = new double[answerChoices.length];
         this.threshold = threshold;
    }
        
    @Override
    public double [] Process()
    {
        freqArray = new double[answerChoices.length];
        int i = 0;
        WhichOfTheseRtn castInfo = (WhichOfTheseRtn)info;
        String [] splitQuestion = castInfo.question.split("\\s+");
        
        //Each basic return represent one of the answer choices.
        for(BasicRtn rtn : castInfo.rtnList)
        {
            for(String s:splitQuestion)
            {
                freqArray[i] += Utilities.count(Utilities.stringArrayToString(rtn.bodyText), s);
            }
             i++;
        }
        boolean belowThresh = true;
        for(double d: freqArray)
            belowThresh = belowThresh && d < threshold;
        if(belowThresh)
        {
            for(int j = 0; j < freqArray.length; j++)
                freqArray[j] = 0;
        }
        return freqArray;
    }
    
}
