/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Processor;

import java.util.ArrayList;
import triviarobot.Logger;
import triviarobot.Logger.Channel;
import triviarobot.Logger.Color;
import triviarobot.Rtn.BasicRtn;
import triviarobot.MiscTools.Utilities;
import static triviarobot.TriviaRobot.L;

/**
 *
 * @author Ol'
 */
public class BasicProcessor extends ProcessorType {
    
    public BasicProcessor(BasicRtn info, String [] answerChoices)
    {
        super(info, answerChoices);
    }
    
    @Override
    public double [] Process()
    {
        L.Log(Channel.ALGORITHMS_MONITER_BASIC_PROCESS, "Starting Processing");
        BasicRtn castInfo = (BasicRtn)info;
        String [] bodyText = castInfo.bodyText;
        ArrayList<ArrayList<String>> answerFragments = new ArrayList<>();
        double [] freqArray = new double[answerChoices.length];
        String[] questionList = castInfo.question.split("\\s+");

        //For each answer choice split the choice up into the different words and store them
        for(int i = 0; i < answerChoices.length; i++)
        {
            answerFragments.add(new ArrayList<String>());
            for(String s: answerChoices[i].split("\\s+"))
            {
                answerFragments.get(i).add(s.toLowerCase());
            }
        }
        L.Log(Channel.ALGORITHMS_MONITER_BASIC_PROCESS, "Fragmented answers: " + answerFragments);
        //For every paragraph of answers
        int i = 0;
        //For every answer
        int [][] questionWordFreq = new int[answerFragments.size()][questionList.length];
        for(ArrayList<String> list: answerFragments)
        {
            boolean addedLog = false;
            //For every word in that answer
            for(String s: list)
            {
                boolean addedLogInner = false;
                if(!((BasicRtn)info).question.contains(" " + s + " "))
                {
                    for(String paragraph: bodyText)
                    {
                      //  L.Log(Channel.ALGORITHMS_MONITER_BASIC_PROCESS, "Processing paragraph: " + paragraph.substring(0, Math.min(100, paragraph.length())) + " ...");
                        if(!paragraph.equals(""))
                        {
                            //Find all indices of that word
                            ArrayList<Integer> indices = Utilities.allIndicesOfWord(paragraph, s);
                            //Add that freq array
                            freqArray[i] += indices.size();
                            //For every index that we found the word at Log it, and color all the keywords red
                            for(Integer j: indices)
                            {
                               addedLog = addedLogInner = true;
                               String message = L.spanNthWord(paragraph, Utilities.charIndexToWordIndex(paragraph, j), "red main");
                               //This is a little bad, the radius has to leave room for the span in it so I upped it to 75 and the 24 accounts 
                               //For the length of the span tag
                               message = Utilities.getRadiusPadded(message, j + 24 + s.length()/2, 75);
                               message = L.colorWords(message, list, Color.red);
                               questionWordFreq[i] = Utilities.arrayAdd(Utilities.arrayFreq(message , questionList), questionWordFreq[i]);
                               message = L.colorWords(message, questionList, Color.blue);
                               L.Log(Channel.GOOGLE_USER_BASIC,message);
                            }
                        }
                    }
                }
                if(addedLogInner)
                    L.blankLines(2, Channel.GOOGLE_USER_BASIC);
            }
            if(addedLog)
                L.blankLines(2, Channel.GOOGLE_USER_BASIC);
            i++;
        }
        for(int j = 0; j < answerFragments.size(); j++)
        {
            int idx = Utilities.maxScoreIndex(questionWordFreq[j]);
            if(idx > -1)
                L.Log(Channel.GOOGLE_USER_SUMMARY,Utilities.stringArrayToString(answerFragments.get(j)) + "(" + questionWordFreq[j][idx] + "): --> " + questionList[idx]);
        }
        
        return freqArray;
    }
}
