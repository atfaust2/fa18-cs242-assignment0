/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Processor;

import triviarobot.Logger;
import triviarobot.Logger.Channel;
import triviarobot.Rtn.RtnType;
import triviarobot.MiscTools.Utilities;
import static triviarobot.TriviaRobot.L;

/**
 *
 * @author Ol'
 */
public abstract class ProcessorType {
    String [] answerChoices;
    RtnType info;
    
    public ProcessorType(RtnType info, String [] answerChoices)
    {
        this.answerChoices = answerChoices;
        this.info = info;
    }
    /*
        Right now, cleaning consists of lower-caseing, removing punctuation (including 's),
        and removing stop words
    */
    public void cleanAnswers()
    {
        L.Log(Channel.ALGORITHMS_MONITOR, "Cleaning Answer Choices");
        String before = "";
        String after = "";
        for(int i = 0; i < answerChoices.length; i++)
        {
            before += answerChoices[i] + "\n";
            answerChoices[i] = answerChoices[i].toLowerCase();
            for(String s: Utilities.STOP_WORDS)
                answerChoices[i] = answerChoices[i].replaceAll("\\b"+s+"\\b", "").trim();
            answerChoices[i] = answerChoices[i].replace("'s", "");
            answerChoices[i] = answerChoices[i].replaceAll("[^a-z0-9\\s]", "");
            answerChoices[i] = answerChoices[i].replaceAll("\\s{2,}", " ").trim();
            after += answerChoices[i] + "\n";
        }
        L.Log(Channel.ALGORITHMS_MONITOR, "Answer Choices Before: \n" + before);
        L.Log(Channel.ALGORITHMS_MONITOR, "Answer Choices After: \n" + after);

    }
    
    abstract double [] Process();
}
