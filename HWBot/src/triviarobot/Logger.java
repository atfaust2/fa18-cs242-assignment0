/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import triviarobot.MiscTools.Utilities;
import static triviarobot.TriviaRobot.L;

/**
 *
 * @author Ol'
 */
public class Logger{
    
    public enum Channel {
    GOOGLE_USER_BASIC_FEATURED,
    GOOGLE_USER_BASIC,
    GOOGLE_USER_WOT,
    ALGORITHMS_MONITOR,
    ALGORITHMS_MONITOR_BASIC,    
    ALGORITHMS_MONITOR_WOT,
    BASIC_ANSWER,
    ERROR_CHANNEL,
    THROW_AWAY,
    TIMER, 
    ALGORITHMS_MONITOR_BASIC_SEARCH, 
    ALGORITHMS_MONITER_BASIC_CLEAN, 
    ALGORITHMS_MONITER_BASIC_PROCESS,
    GOOGLE_USER_SUMMARY
    }
    public boolean steamOutput = false;
    public boolean timeStamp = false;
    private long startTime = System.currentTimeMillis();
    public ArrayList<Channel> blocked = new ArrayList<>();
    
    public enum Color {
        red,
        blue,
    }
    public static StringBuffer Logs = new StringBuffer();
    
    public void block(Channel c)
    {
        blocked.add(c);
    }
    public void blockAll()
    {
        for(Channel c: Channel.values())
        {
            block(c);
        }
    }
    public void unBlock(Channel c)
    {
        blocked.remove(c);
    }
    
    public void block(ArrayList<Channel> channels)
    {
        for(Channel c: channels)
            block(c);
    }
    
    public void block(Channel [] channels)
    {
        for(Channel c: channels)
            block(c);
    }
    
    public static void print()
    {
        System.out.print(Logs);
        Logs = new StringBuffer();
    }
    
    public void blankLines(int i)
    {
        for(int j = 0; j < i;  j++)
            Logs.append("\n");
    }
    public void blankLines(int i, Channel c)
    {
        if(!blocked.contains(c))
        {
            for(int j = 0; j < i;  j++)
                Logs.append("[" + c.toString() + "]\n");
            print();
        }
    }
    public void Log(Channel c, String message)
    {
        if(!blocked.contains(c))
        {
            if(timeStamp)
                Logs.append("[" + c.toString() + "]" + "[" + (System.currentTimeMillis() - startTime) + "]"+ message.replace("\n","\n[" + c.toString() + "]" ) + "\n");
            else
                Logs.append("[" + c.toString() + "]" + message.replace("\n","\n[" + c.toString() + "]" ) + "\n");

                if(this.steamOutput)
                    print();
        }
    }
    
    //
    public String span(String s, String className)
    {
        return "<span" + " class= \"" + className + "\">" + s + "</span>";
    }
    public String spanAndIndex(String s, String className, int index)
    {
        return "<span" + " class= \"" + className + "\"" + " data-index= \"" + index + "\">" + s + "</span>";
    }
    
    public String colorWord(String in, String s, Color c)
    {
        return in.replace(" " + s + " ", " " + span(s, c.toString()) + " ");
    }
    
    public String colorWords(String in, String [] words, Color c)
    {
        String rtn = in;
        for(String s: words)
            rtn = colorWord(rtn, s, c);
        return rtn;
    }
    public String colorWords(String in, ArrayList<String> words, Color c)
    {
        String rtn = in;
        for(String s: words)
            rtn = colorWord(rtn, s, c);
        return rtn;
    }
    public String colorNthWord(String in, int i, Color c)
    {
        String[] words = in.split("\\s+");
        words[i] = span(words[i], c.toString());
        return Utilities.stringArrayToString(words);
    }
    public String spanNthWord(String in, int i, String span)
    {
        String[] words = in.split("\\s+");
        if(i >= words.length)
        {
            L.Log(Channel.ERROR_CHANNEL,"Call of span on " + in + " " + i + " is invalid");
            return in;
        }
        
        words[i] = span(words[i], span);
        return Utilities.stringArrayToString(words);
    }
    public String spanNthWordWithIndex(String in, int i, String span, int idx)
    {
        String[] words = in.split("\\s+");
        if(i >= words.length)
        {
            L.Log(Channel.ERROR_CHANNEL,"Call of span on " + in + " " + i + " is invalid");
            return in;
        }
        
        words[i] = spanAndIndex(words[i], span, idx);
        return Utilities.stringArrayToString(words);
    }
}
