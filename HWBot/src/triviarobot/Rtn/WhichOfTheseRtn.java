/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Rtn;

/**
 *
 * @author Ol'
 */
public class WhichOfTheseRtn extends RtnType{
    
    public String question;
    public BasicRtn [] rtnList;
    
    
    public WhichOfTheseRtn(String question, BasicRtn [] rtnList)
    {
        this.question = question;
        this.rtnList = rtnList;
    }
    
    @Override
    public void Clean()
    {
        for(BasicRtn rtn: rtnList)
        {
            rtn.Clean();
        }
        question = genericClean(question);
    }
   
    
    
}
