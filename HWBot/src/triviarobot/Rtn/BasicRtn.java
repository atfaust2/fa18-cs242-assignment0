/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Rtn;
import triviarobot.Logger.Channel;
import static triviarobot.TriviaRobot.L;

/**
 *
 * @author Ol'
 */
public class BasicRtn extends RtnType{
    public String featuredText;
    public final String [] URLs;
    public final String [] bodyText;
    public String question;
    
    public BasicRtn(String featuredText, String [] URLs, String [] summaryText, String question)
    {
        this.featuredText = featuredText;
        this.URLs = URLs;
        this.bodyText = summaryText;
        this.question = question;
    }
    /*
        For this type, Clean means the following:
            URL's are left alone in case we need use them in the future to look something up
            Featured Text has punctuation removed, 's removed, and is lower cased
            Summary text has punctuation, 's and stop words removed and is lower cased
    */
    @Override
    public void Clean()
    {
        L.Log(Channel.ALGORITHMS_MONITER_BASIC_CLEAN, "Starting Clean");
        L.Log(Channel.ALGORITHMS_MONITER_BASIC_CLEAN, "Question Before: " + question);
        cleanBodyText();
        cleanQuestion();
        L.Log(Channel.ALGORITHMS_MONITER_BASIC_CLEAN, "Question Before: " + question);
    }
    
    void cleanBodyText()
    {
        for(int i = 0; i < bodyText.length; i ++)
        {
            bodyText[i] = genericClean(bodyText[i]);
        }
    }
    
    void cleanQuestion()
    {
        question = genericClean(question);
    }
}
