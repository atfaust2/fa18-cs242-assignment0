package triviarobot.Rtn;

import triviarobot.MiscTools.Utilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ol'
 */
public abstract class RtnType {
        
    public abstract void Clean();
    
    String genericClean(String in)
    {
        String rtn = in;
        //Remove 's
        rtn = rtn.replace("'s", "");
        //Remove punctuation
        rtn = rtn.replaceAll("[^a-zA-Z0-9\\s]", " ");
        //Lower case
        rtn = rtn.toLowerCase();
        //Remove stop words
        for(String s: Utilities.STOP_WORDS)
            rtn = rtn.replaceAll("\\b"+s+"\\b", "");
        //Take out any double spaces we created
        rtn = rtn.replaceAll("\\s{2,}", " ");
        //Trim any remaining whitespace
        rtn = rtn.trim();
        
        return rtn;
    }
}
