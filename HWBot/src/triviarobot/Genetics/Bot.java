/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Genetics;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import triviarobot.Question;
import triviarobot.TriviaRobot;
import triviarobot.MiscTools.Utilities;
import static triviarobot.TriviaRobot.ACCURACY;
import static triviarobot.TriviaRobot.Correct;
import static triviarobot.TriviaRobot.IDK;
import static triviarobot.TriviaRobot.getAnswer;
import static triviarobot.TriviaRobot.incorrect;

/**
 *
 * @author Ol'
 */
public class Bot {
    double fitness = 0;
    ArrayList<Boolean> genes = new ArrayList<>();
    double accuracy = 0;
    double WOTThresh = 0;
    double RevThresh = 0;
    public Bot()
    {
        Random rn  = new Random();
        
        for(int i = 0; i < Utilities.topWords.length + TriviaRobot.numMethods; i++)
        {
            genes.add(rn.nextBoolean());
        }
        accuracy = Math.random()*(2.0/3) + 1.0/3;
        WOTThresh = Math.random() * 10;
        RevThresh = Math.random() * 10;
        fitness = 0;
    }
    public Bot(Bot b)
    {
        this.fitness = b.fitness;
        this.accuracy = b.accuracy;
        this.WOTThresh = b.WOTThresh;
        this.RevThresh = b.RevThresh;
        for(int i = 0; i < b.genes.size(); i ++)
        {
            genes.add(b.genes.get(i));
        }
        
    }
    
    //Calculates fitness on given i questions
    public void calcFitness(ArrayList<Integer> questions) throws Exception
    {
            fitness = simulateGame(questions);
    }
    
    
    public double simulateGame(ArrayList<Integer> questionNumbers) throws Exception
    {
        double correct = 0;
        double inconclusive = 0;
        double notCorrect = 0;
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        JSONArray ja = (JSONArray) obj;
        for(int i = 0; i < questionNumbers.size(); i ++)
        {
            System.out.print("\tAnswering Question " + (i + 1) + ":");
            //This is the boiler plate json extraction code;
            JSONObject jo = (JSONObject)ja.get(questionNumbers.get(i));

            JSONObject options = (JSONObject)jo.get("options");

            String question = (String)jo.get("question");

            String option1 = (String)options.get("1");

            String option2 = (String)options.get("2");

            String option3 = (String)options.get("3");
            
            long answer = (long)jo.get("answer");
            String[] input = {question, option1, option2, option3};
            Question.badWords = Utilities.boolVectorToArray(new ArrayList<Boolean>(genes.subList(
                    0, Utilities.topWords.length)));
            int prediction = getAnswer(input, false, genes.get(genes.size() - 2), genes.get(genes.size() - 1), WOTThresh, RevThresh);
            if(prediction == answer - 1)
            {
                correct++;
                System.out.println("\t\t\tCorrect");
            }
            else if (prediction == -1)
            {
                inconclusive++;
                System.out.println("\t\t\tIDK");
            }
            else
            {
                System.out.println("\t\t\tIncorrect");
                notCorrect++;
            }
        }
        return Math.pow(accuracy, correct) * Math.pow(.33333333, inconclusive) * Math.pow((1-accuracy)/2, notCorrect);

    }
    public String toString()
    {
        return "Genes:" + genes.toString() + "\naccuracy:" + accuracy + "\nfitness:" + fitness + "\nWOTThresh:" + WOTThresh + "\nRevThresh" + RevThresh + "\n";
    }

}
