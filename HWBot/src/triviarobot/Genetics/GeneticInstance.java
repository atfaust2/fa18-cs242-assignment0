/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Genetics;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Ol'
 */
public class GeneticInstance {
    public Population population;
    public double mutationRate;
    public double mutationSize;
    
    public GeneticInstance(Population population, double mutationRate,double mutationSize)
    {
        this.population = population;
        this.mutationRate = mutationRate;
        this.mutationSize = mutationSize;
    }
    
    private void crossover_helper(Bot b1, Bot b2)
    {
        Random rn = new Random();
        //Select a random crossover point
        int crossOverPoint = rn.nextInt(population.geneSize);

        //Swap values among parents
        for (int i = 0; i < crossOverPoint; i++) {
            boolean temp = b1.genes.get(i);
            b1.genes.set(i,b2.genes.get(i));
            b2.genes.set(i, temp);
        }
        double weight1 = rn.nextDouble();
        double weight2 = rn.nextDouble();
        double acc1 = b1.accuracy;
        double acc2 = b2.accuracy;
        b1.accuracy = acc1 * weight1 + acc2*(1-weight1);
        b2.accuracy = acc2 * weight2 + acc1*(1-weight2);
        double WOTThresh1 = b1.WOTThresh;
        double WOTThresh2 = b2.WOTThresh;
        weight1 = rn.nextDouble();
        weight2 = rn.nextDouble();
        b1.WOTThresh = WOTThresh1 * weight1 + WOTThresh2*(1-weight1);
        b2.WOTThresh = WOTThresh2 * weight2 + WOTThresh1*(1-weight2);
        double RevThresh1 = b1.RevThresh;
        double RevThresh2 = b2.RevThresh;
        weight1 = rn.nextDouble();
        weight2 = rn.nextDouble();
        b1.RevThresh = RevThresh1 * weight1 + RevThresh2*(1-weight1);
        b2.RevThresh = RevThresh2 * weight2 + RevThresh1*(1-weight2);
    }
    
    void mutation() 
    {
        Random rn = new Random();

        for(Bot b: population.bots)
        {
            if(rn.nextDouble() < mutationRate)
            {
                for(int i = 0; i < mutationSize; i++)
                {
                    //Select a random mutation point
                    int mutationPoint = rn.nextInt(population.geneSize);

                    //Flip values at the mutation point

                    if (b.genes.get(mutationPoint) == false) 
                    {
                        b.genes.set(mutationPoint,true);
                    } 
                    else 
                    {
                        b.genes.set(mutationPoint,false);
                    }
                }
                b.accuracy *= (rn.nextDouble() *.4 + .8);
                b.WOTThresh *= (rn.nextDouble() *.4 + .8);
                b.RevThresh *= (rn.nextDouble() *.4 + .8);
            }
        }
    }
    public void crossOver()
    {
        Random rn = new Random();
        ArrayList<Integer> botNums = new ArrayList<>();
        for(int i = 0; i < population.bots.size(); i++)
        {
            botNums.add(i);
        }
        while(botNums.size() >  1)
        {
            int idx1 = rn.nextInt(botNums.size());
            Bot mate1 = population.bots.get(botNums.get(idx1));
            botNums.remove(idx1);
            int idx2 = rn.nextInt(botNums.size()); 
            Bot mate2 = population.bots.get(botNums.get(idx2));
            Bot b1 = new Bot(mate2);
            Bot b2 = new Bot(mate1);
            crossover_helper(b1, b2);
            botNums.remove(idx2);
            population.bots.add(b1);
            population.bots.add(b2);
            population.populationSize += 2;
        }
        
    }
    
    

}
