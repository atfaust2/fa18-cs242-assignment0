/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Genetics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import triviarobot.MiscTools.Utilities;

/**
 *
 * @author Ol'
 */
public class Population {
    int populationSize = 15;
    ArrayList<Bot> bots = new ArrayList<>();
    int geneSize = 0;
    
    public Population(int populationSize, int geneSize)
    {
       this.populationSize = populationSize;
        for(int i = 0; i < populationSize; i++)
            bots.add(new Bot());
        this.geneSize = geneSize;
    }
    
    private void sortBotsByFitness(int numTrials) throws Exception
    {
        ArrayList<Integer> questionNumbers = Utilities.randomVector(numTrials, 0, 800, true);
        for(int i = 0; i < bots.size(); i ++)
        {
            System.out.println("Bot " + (i + 1) + " running");
            
            bots.get(i).calcFitness(questionNumbers);
            
            System.out.print(bots.get(i));
        }
        Collections.sort(bots, new Comparator<Bot>() {
            @Override
            public int compare(Bot b2, Bot b1) 
            {
                return Double.compare(b1.fitness, b2.fitness);
            }
        });
    }
    public ArrayList<Bot> getFittest(int numBots, int numTrials) throws Exception
    {
        if(numBots <= 0 || numTrials <= 0)
            return null;
        sortBotsByFitness(numTrials);
        bots = new ArrayList<Bot>(bots.subList(0, numBots));
        this.populationSize = numBots;
        return new ArrayList<Bot>(bots.subList(0, numBots));
    } 
    
    public String toString()
    {
        String s = "";
        for(Bot b: bots)
            s += b.toString();
        return "Size: " + populationSize + "\n" + s;
    }
}
