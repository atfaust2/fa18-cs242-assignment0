/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot;

import triviarobot.MiscTools.Utilities;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.TreeSet;
import org.jsoup.Jsoup;  
import org.jsoup.nodes.Document;  
import org.jsoup.nodes.Element;

/**
 *
 * @author Ol'
 */
public class Question {
    private String question;
    private ArrayList<String> answers;
    private boolean searched = false;
    private String featured = "";
    boolean not = false;
    private int totalHits = -1;
    ArrayList<String> wordsList = null;
    ArrayList<Double> frequencies = null;
    private boolean basicAnswerFound = false;
     public static TreeSet<String> badWords = Utilities.boolVectorToArray(Utilities.inclusionVector);
  //  public static TreeSet<String> badWords = new TreeSet<String>(Arrays.asList("which", "of", "these", "an", "a", "of", "is", "typically", "what", "s", "and"));

    
    public Question(String question, ArrayList<String> answers)
    {
        this.question = question.toLowerCase();
        this.not = this.question.contains(" not ");
        ListIterator<String> iterator = answers.listIterator();
        while (iterator.hasNext())
        {
            iterator.set(iterator.next().toLowerCase());
        }
        this.answers = answers;
        this.not = question.contains(" not ");
    }
    //Pass in a list with the quesiton as the first element and the answer choices as the rest
    public Question(ArrayList<String> input)
    {
        this.question = input.get(0).toLowerCase();
        this.not = this.question.contains(" not ");
        input.remove(0);
        ListIterator<String> iterator = input.listIterator();
        while (iterator.hasNext())
        {
            iterator.set(iterator.next().toLowerCase());
        }
    }
    public Question(String [] input)
    {
        this.question = input[0].toLowerCase();
        this.not = this.question.contains(" not ");
        this.answers = new ArrayList<>();
        for(int i = 1; i < input.length; i++)
            this.answers.add(input[i].toLowerCase());
    }
    public Question(String question, String [] answers)
    {
        this.question = question.toLowerCase();
        this.answers = new ArrayList<String>(Arrays.asList(answers));
    }
    public ArrayList<Double> getBasicAnswer() throws Exception
    {
        if(basicAnswerFound)
            return frequencies;
        ArrayList<Double> rtn = new ArrayList<>();
        //Search the question
        String s = googleSearch(question);
        //Break apart the results into a list of all the words
        String[] words = s.replaceAll("[^a-zA-Z ]", " ").toLowerCase().trim().split("\\s+");
        wordsList = queryAndClean(question);
        //An accumulator that tells how many total hits we got on all words comvined, and a freq list that tells you how many hits we got on each answer choice
        int max = 0;
        //For every answer check how many times it showed up in the search. If multiple words in one answer then sum the freq.
        ArrayList<String> cleanAnswers = Utilities.destroyCommonalities(answers);
        for(int i = 0; i < answers.size(); i++)
        {
            rtn.add(0.0);
            //For every word in the answer choice, First split it up
            String[] split_answer = cleanAnswers.get(i).replaceAll("[^a-zA-Z]", " ").toLowerCase().trim().split("\\s+");
            //For every word in the answer find the frequency of it in the query and then sum them all together
            int potentialMax = 0;
            for(int j = 0; j < split_answer.length; j++)
            {
                //filter out small words and "bad" words
                if(!badWords.contains(split_answer[j]))
                {
                    ArrayList<Integer> indices = Utilities.allIndicesOf(wordsList, split_answer[j]);
                    int freq = indices.size();
                    //Try to find it in the original google search
                    rtn.set(i, rtn.get(i) + freq);
                    potentialMax += freq;
              /*      System.out.println("[Google User]For Answer: " + cleanAnswers.get(i) + "\n[Google User]\tsubWord:" + split_answer[j]);
                    for(Integer k: indices)
                       System.out.println("[Google User]" + Utilities.stringArrayToString(new ArrayList<String>(wordsList.subList(Math.max(0, k - 10), Math.min(k + 10, wordsList.size() - 1)))).replace(split_answer[j],split_answer[j].toUpperCase()));
                */
                }
            }
            if(potentialMax > max)
                max = potentialMax;
        }
        if(not)
        {
            for(int i = 0; i < rtn.size(); i++)
            {
                rtn.set(i, max - rtn.get(i));
            }
        }
        max = 0;
        this.totalHits = (int)Utilities.arraySum(rtn);
        this.frequencies = rtn;
        return rtn;
    }
    
    
    public ArrayList<Double> getBasicAnswer(double threshold) throws Exception
    {
        if(basicAnswerFound)
            return frequencies;
        ArrayList<Double> rtn = new ArrayList<>();
        //Search the question
        String s = googleSearch(question);
        //Break apart the results into a list of all the words
        String[] words = s.replaceAll("[^a-zA-Z ]", " ").toLowerCase().trim().split("\\s+");
        wordsList = queryAndClean(question);
        //An accumulator that tells how many total hits we got on all words comvined, and a freq list that tells you how many hits we got on each answer choice
        int hits = 0;
        int max = 0;
        //For every answer check how many times it showed up in the search. If multiple words in one answer then sum the freq.
        ArrayList<String> cleanAnswers = Utilities.destroyCommonalities(answers);
        for(int i = 0; i < answers.size(); i++)
        {
            rtn.add(0.0);
            //For every word in the answer choice, First split it up
            String[] split_answer = cleanAnswers.get(i).replaceAll("[^a-zA-Z]", " ").toLowerCase().trim().split("\\s+");
            //For every word in the answer find the frequency of it in the query and then sum them all together
            int potentialMax = 0;
            for(int j = 0; j < split_answer.length; j++)
            {
                //filter out small words and "bad" words
                if(split_answer[j].length() > 3 && !badWords.contains(split_answer[j]))
                {
                    int fr1 = Utilities.allIndicesOf(wordsList, split_answer[j]).size();
                    int freq = Collections.frequency(wordsList, split_answer[j]);
                    //Try to find it in the original google search
                    rtn.set(i, rtn.get(i) + freq);
                    hits += freq;
                    potentialMax += freq;
                }
            }
            if(potentialMax > max)
                max = potentialMax;
        }
        if(not)
        {
            hits = 0;
            for(int i = 0; i < rtn.size(); i++)
            {
                rtn.set(i, max - rtn.get(i));
                hits += rtn.get(i);
            }
        }
        max = 0;
     //   for(int i = 0; i < rtn.size(); i ++)
     //   {
     //       
     //       rtn.set(i, rtn.get(i)/hits);
     //       if(rtn.get(i) > rtn.get(max))
     //           max = i;
     //   }
     //   System.out.println("Answer: " + answers.get(max));
        this.frequencies = rtn;
        rtn = Utilities.arrayRailu(Utilities.arraySub(rtn, threshold));
        this.totalHits = (int)Utilities.arraySum(rtn);
        return rtn;
        
    }
    
    
    public String getQuestion()
    {
        return question;
    }
    public ArrayList<String> getAnswers()
    {
        return answers;
    }
    public String getAnswer(int i)
    {
        return answers.get(i);
    }
    public String getFeatured() throws Exception
    {
        if(!searched)
        {
            googleSearch(question);
            return featured;
        }
        else
        {
            return featured;
        }
    }
    
    public int getTotalHits() throws Exception
    {
        if(searched)
            return totalHits;
        else
        {
            getBasicAnswer();
            return totalHits;
        }
    }
    
    public int getTotalHits(double threshold) throws Exception
    {
        if(searched)
            return totalHits;
        else
        {
            getBasicAnswer(threshold);
            return totalHits;
        }
    }
    
    public ArrayList<String> queryAndClean(String query) throws Exception
    {
        query = query.replaceAll("[^a-zA-Z0-9]", " ").toLowerCase().trim();
     //   System.out.println("[Algorithms Monitor]Asked to Query: " + query);
        String[] querySplit = query.split("\\s+");
        ArrayList<String> queryList = new ArrayList<String>(Arrays.asList(querySplit));
        queryList.removeAll(badWords);
        String realquery = Utilities.stringArrayToString(queryList);
   //     System.out.println("[Algorithms Monitor]Actually Query:" + realquery);
        String s = googleSearch(realquery);
        String[] words = s.replaceAll("[^a-zA-Z ]", " ").toLowerCase().trim().split("\\s+");
        ArrayList<String> rtn = new ArrayList<String>(Arrays.asList(words));
        return rtn;

    }
    
    private String googleSearch(String query) throws Exception
    {
        searched = true;
        String queryResult = "";
        String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) C"
                + "hrome/64.0.3282.186 Safari/537.36";
                Document doc;
                while(true)
                {
                    try
                    {
                        doc = Jsoup.connect("https://google.com/search?q=" + query).userAgent(USER_AGENT).timeout(100000).get();
                        break;
                    }
                    catch(org.jsoup.HttpStatusException e)
                    {
                        System.out.println("The call to google failed, you probably got locked out for submitting the same question too many times. Press 1 to try to continue");
                        Scanner sc =new Scanner(System.in);  
                        while(!sc.next().equals("1"))
                        {
                            if(sc.next().equals("print") || sc.next().equals("p") || sc.next().equals("2"))
                            {
                                System.out.println("Incorrect");
                                for(String s :TriviaRobot.incorrect)
                                    System.out.println(s);
                                System.out.println("Correct");
                                for(String s :TriviaRobot.Correct)
                                    System.out.println(s);
                                System.out.println("IDK");
                                for(String s :TriviaRobot.IDK)
                                    System.out.println(s);
                                
                            }
                        }
                        
                    }
                }
        //Traverse the results
        //for (Element result : doc.select("div#resultStats")){
            //resultInfo += result.text() + "\n";
        //}
        for(Element result : doc.select(".Z0LcW"))
        {
            featured += result.text();
        }
        for (Element result : doc.select("h3.r a, span.st, span.st + div")){
            if(result.attr("href").contains("http")) 
            {}
            else if (result.text().contains("Missing"))
            {}
            else
            {
                queryResult += result.text() + "\n";
            }

        }
        return queryResult;
    }
    public ArrayList<Double> WhichOfTheseSearch() throws Exception
    {
        ArrayList<Double> rtn = new ArrayList<Double>();
        for(String s: answers)
        {
       //     System.out.println(question.toLowerCase().replace("which of these","").replace("?", "").replaceAll("\\b\\w{1,2}\\b","")+ " " + s);
            String result = googleSearch(question.toLowerCase().replace("which of these","").replace("?", "").replaceAll("\\b\\w{1,2}\\b","")+ " " + s);
            int freq = Utilities.count(result.toLowerCase(), s.toLowerCase());
            rtn.add(0.0 + freq);
            double max = Collections.max(rtn);
            if(not)
            {
                for(int i = 0; i < rtn.size(); i++)
                    rtn.set(i, max - rtn.get(i));
            }
        }
        return rtn;
    }
    
        public ArrayList<Double> WhichOfTheseSearch(double threshold) throws Exception
    {
        ArrayList<Double> rtn = new ArrayList<Double>();
        for(String s: answers)
        {
       //     System.out.println(question.toLowerCase().replace("which of these","").replace("?", "").replaceAll("\\b\\w{1,2}\\b","")+ " " + s);
            String result = googleSearch(question.toLowerCase().replace("which of these","").replace("?", "").replaceAll("\\b\\w{1,2}\\b","")+ " " + s);
            int freq = Utilities.count(result.toLowerCase(), s.toLowerCase());
            rtn.add(0.0 + freq);
            double max = Collections.max(rtn);
            if(not)
            {
                for(int i = 0; i < rtn.size(); i++)
                    rtn.set(i, max - rtn.get(i));
            }
        }
        return Utilities.arrayRailu(Utilities.arraySub(rtn,threshold));
    }
    
    @Override
    public String toString() {
        String s =  String.format(this.question + "\n");
        for(int i = 0; i < answers.size(); i++)
        {
            s += i + ".) " + Utilities.destroyCommonalities(answers).get(i) + "\n";
        }
        return s;
    }
}
    