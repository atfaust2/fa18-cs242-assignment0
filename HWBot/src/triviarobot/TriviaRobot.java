/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot;

import triviarobot.MiscTools.Utilities;
import triviarobot.Algorithm.BasicAlgorithmConcurrent;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import triviarobot.Algorithm.BasicAlgorithm;
import triviarobot.Algorithm.WhichOfTheseAlgorithm;

/**
 *
 * @author Ol'
 */

public class TriviaRobot {
    public static final double ACCURACY = .65;
    public static final int numMethods = 3;
    public static String global_question = "";
    /**
     * @param args the command line arguments
     */
    public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    public static Logger L = new Logger();
    public static ArrayList<String> incorrect = new ArrayList<String>();
    public static ArrayList<String> Correct = new ArrayList<String>();
    public static ArrayList<String> IDK = new ArrayList<String>();
    public static void main(String[] args) throws Exception{
       // double [] delay = {1000, 10000, 60000};
       // double [] questionMod = {1, 12, 40};
        //tester(true,"C:\\Users\\Ol'", questionMod, delay);
  //      runRobot(args);
       /* simulateGame(12);
        Boolean [] list1 = {true, false, true, true, false, false, true, false, true, true, true, false, false, true, true, false, false, false, true, true, true, false, true, true, false, true, false, true, false, true, true, true, true, true, false, false, false, true, false, false, true, false, true, false, false, true, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, false, true, true, true, false, false, false, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, true, true, false, false, true, false, false, true, false, true, false, true, true, false, false, false, true, true, true, false, true, false, true, true, true, true, true, false, false, false, false, true, false, false, true, false, false, false, true, true, true, false, false, true, true, false, true, false, true, false, true, true, false, true, false, true, false, false, true, true, false, true, true, false, false, false, false, false, false, true, false, true, false, true, false, false, true, true, true, true, false, false, false, true, false, false, false, false, true, false, false, true, false, true, true, false, true, false, false, true, true, true, false, false, true, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, false, true, false, false, false, false, true, true, true, false, true, false, false, true, true, true, false, true, false, false, true, false, false, true, true, true, true, false, true, true};
        Boolean [] list2 = {true, false, true, true, false, false, true, false, true, true, true, false, false, true, true, false, false, false, true, true, true, false, true, true, false, true, false, true, false, true, true, true, true, true, false, false, false, true, false, false, true, false, true, false, false, true, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, false, true, true, true, false, false, false, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, true, true, false, false, true, false, false, true, false, true, false, true, true, false, false, false, true, true, true, false, true, false, true, true, true, true, true, false, false, false, false, true, false, false, true, false, false, false, true, true, true, false, false, true, true, false, true, false, true, false, true, true, false, true, false, true, false, false, true, true, false, true, true, false, false, false, false, false, false, true, false, true, false, true, false, false, true, true, true, true, false, false, false, true, false, false, false, false, true, false, false, true, false, true, true, false, true, false, false, true, true, true, false, false, true, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, false, true, false, false, false, false, true, true, true, false, true, false, false, true, true, true, false, true, false, false, true, false, false, true, true, true, true, false, true, true};
        Boolean [] list3 = {true, false, true, true, false, false, true, false, true, true, true, false, false, true, true, false, false, false, true, true, true, false, true, true, false, true, false, true, false, true, true, true, true, true, false, false, false, true, false, false, true, false, true, false, false, true, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, false, true, true, true, false, false, false, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, true, true, false, false, true, false, false, true, false, true, false, true, true, false, false, false, true, true, true, false, true, false, true, true, true, true, true, false, false, false, false, true, false, false, true, false, false, false, true, true, true, false, false, true, true, false, true, false, true, false, true, true, false, true, false, true, false, false, true, true, false, true, true, false, false, false, false, false, false, true, false, true, false, true, false, false, true, true, true, true, false, false, false, true, false, false, false, false, true, false, false, true, false, true, true, false, true, false, false, true, true, true, false, false, true, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, false, true, false, false, false, false, true, true, true, false, true, false, false, true, true, true, false, true, false, false, true, false, false, true, true, true, true, false, true, true};
        Boolean [] list4 = {true, false, true, true, false, false, true, false, true, true, true, false, false, true, true, false, false, false, true, true, true, false, true, true, false, true, false, true, false, true, true, true, true, true, false, false, false, true, false, false, true, false, true, false, false, true, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, false, true, true, true, false, false, false, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, true, true, false, false, true, false, false, true, false, true, false, true, true, false, false, false, true, true, true, false, true, false, true, true, true, true, true, false, false, false, false, true, false, false, true, false, false, false, true, true, true, false, false, true, true, false, true, false, true, false, true, true, false, true, false, true, false, false, true, true, false, true, true, false, false, false, false, false, false, true, false, true, false, true, false, false, true, true, true, true, false, false, false, true, false, false, false, false, true, false, false, true, false, true, true, false, true, false, false, true, true, true, false, false, true, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, false, true, false, false, false, false, true, true, true, false, true, false, false, true, true, true, false, true, false, false, true, false, false, true, true, true, true, false, true, true};
        Boolean [] list5 = {false, true, true, false, false, false, true, false, false, false, true, true, true, true, false, false, true, false, false, true, false, true, false, false, true, false, false, true, true, true, false, false, true, false, false, false, false, true, false, false, true, false, true, false, false, true, true, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, true, true, true, false, false, true, true, false, false, true, true, true, false, false, true, false, false, false, true, false, false, true, false, false, true, false, true, false, true, true, false, false, false, true, true, true, false, true, false, true, true, true, true, true, false, false, false, false, true, false, false, true, false, false, false, true, true, true, false, false, true, true, false, true, false, true, false, true, true, false, true, false, true, false, false, true, true, false, true, true, false, false, false, false, false, false, true, false, true, false, true, false, false, true, true, true, true, false, false, false, true, false, false, false, false, true, false, false, true, false, true, true, false, true, false, false, true, true, true, false, false, true, false, true, true, false, true, true, true, false, true, false, true, false, true, true, true, false, true, false, false, false, false, true, true, true, false, true, false, false, true, true, true, false, true, false, false, true, false, false, true, true, true, true, false};
        Boolean [] list6 = {false, false, false, true, false, true, false, false, false, true, true, false, false, true, false, true, false, true, true, true, true, false, true, false, false, true, true, true, false, false, false, true, false, false, true, true, false, true, true, false, true, false, false, true, true, true, true, true, false, false, false, false, true, false, true, false, false, false, true, false, false, true, true, true, true, false, true, false, false, false, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false, false, false, true, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, false, false, true, true, false, true, false, false, false, false, false, true, true, false, false, true, true, false, false, true, false, true, true, true, true, false, false, false, false, true, true, true, true, true, true, false, true, true, true, false, false, true, false, false, true, true, false, false, true, false, true, true, true, false, true, false, true, false, false, false, false, false, false, true, true, true, false, true, true, false, false, false, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, true, false, true, false, false, false, false, false, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, true, true, true, false, false, true, true, false, false, true, false, true, false, true, false};
        Boolean [] list7 = {false, false, false, true, true, true, false, false, false, true, true, false, false, true, false, true, false, true, true, true, true, false, true, false, false, true, true, true, false, false, false, true, false, false, true, true, false, true, true, false, true, false, false, true, true, true, true, true, false, false, false, false, true, false, true, false, false, false, true, false, false, true, true, true, true, false, true, false, false, false, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false, false, false, true, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, false, false, true, true, false, true, false, false, false, false, false, true, true, false, true, true, true, false, false, false, true, true, false, true, true, false, true, false, false, true, false, true, true, true, true, false, true, true, true, false, false, true, false, false, true, true, false, false, true, false, true, true, true, false, true, false, true, false, false, false, false, false, false, true, true, true, false, true, true, false, false, false, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, true, false, true, false, false, false, false, false, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, true, true, true, false, false, true, true, false, false, true, false, true, false, true, false};
        Boolean [] list8 = {false, false, false, true, true, true, false, false, false, true, true, false, false, true, false, true, false, true, true, true, true, false, true, false, false, true, true, true, false, false, false, true, false, false, true, true, false, true, true, false, true, false, false, true, true, true, true, true, false, false, false, false, true, false, true, false, false, false, true, false, false, true, true, true, true, false, true, false, false, false, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false, false, false, true, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, false, false, true, true, false, true, false, false, false, false, false, true, true, false, true, true, true, false, false, false, true, true, false, true, true, false, true, false, false, true, false, true, true, true, true, false, true, true, true, false, false, true, false, false, true, true, false, false, true, false, true, true, true, false, true, false, true, false, false, false, false, false, false, true, true, true, false, true, true, false, false, false, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, true, false, true, false, false, false, false, false, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, true, true, true, false, false, true, true, false, false, true, false, true, false, true, false};
        Boolean [] list9 = {false, false, false, true, true, true, false, false, false, true, true, false, false, true, false, true, false, true, true, true, true, false, true, false, false, true, true, true, false, false, false, true, false, false, true, true, false, true, true, false, true, false, false, true, true, true, true, true, false, false, false, false, true, false, true, false, false, false, true, false, false, true, true, true, true, false, true, false, false, false, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false, false, false, true, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, false, false, true, true, false, true, false, false, false, false, false, true, true, false, true, true, true, false, false, false, true, true, false, true, true, false, true, false, false, true, false, true, true, true, true, false, true, true, true, false, false, true, false, false, true, true, false, false, true, false, true, true, true, false, true, false, true, false, false, false, false, false, false, true, true, true, false, true, true, false, false, false, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, true, false, true, false, false, false, false, false, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, true, true, true, false, false, true, true, false, false, true, false, true, false, true, false};
        Boolean [] list10 = {false, false, false, true, false, true, false, false, false, true, true, false, false, true, false, true, false, true, true, true, true, false, true, false, false, true, true, true, false, false, false, true, false, false, true, true, false, true, true, false, true, false, false, true, true, true, true, true, false, false, false, false, true, false, true, false, false, false, true, false, false, true, true, true, true, false, true, false, false, false, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false, false, false, true, false, true, true, false, true, false, true, true, true, true, false, true, false, true, true, false, false, true, true, false, true, false, false, false, false, false, true, true, false, true, true, true, false, false, false, true, true, false, true, true, false, true, false, false, true, false, true, true, true, true, false, true, true, true, false, false, true, false, false, true, true, false, false, true, false, true, true, true, false, true, false, true, false, false, false, false, false, false, true, true, true, false, true, true, false, false, false, false, true, true, false, false, true, false, false, true, true, true, true, false, false, false, false, true, false, true, false, false, false, false, false, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, true, true, true, false, false, true, true, false, false, true, false, true, false, true, false,};
        System.out.println(Utilities.boolVectorToArray(Utilities.inclusionVector));
        */
        
      /*  ArrayList<Boolean[]> genesListList = new ArrayList<>();
        genesListList.add(list1);
        genesListList.add(list2);
        genesListList.add(list3);
        genesListList.add(list4);
        genesListList.add(list5);
        genesListList.add(list6);
        genesListList.add(list7);
        genesListList.add(list8);
        genesListList.add(list9);
        genesListList.add(list10);
        System.out.println(Utilities.compileResults(genesListList));
        System.out.println(list1.length);*/
   //     System.out.println(Utilities.topWords.length);
   //     Population p = new Population(20, Utilities.topWords.length + numMethods);
   //     GeneticInstance g = new GeneticInstance(p, .15, 10);
   //     for(int i = 0; i < 20; i ++)
   //     {
    //        System.out.println(p.getFittest(5, 12));
    //        g.crossOver();
    //        g.mutation();
    //    }
    /*    String [] answers = {"George Bush", "George Washington", "Jack Daniels"};
        BasicSearch b = new BasicSearch("Who was the first president");
        b.initialize();
        BasicRtn br = b.getProcessedResults();
        BasicProcessor bp = new BasicProcessor(br, answers);
        bp.initialize();
        for(double d: bp.getFreqArray())
            System.out.println(d); */
     /*   String [] questions = {"Who was the first president of the Untied States of America?", "Who sings wrecking ball?", "Who wrote green eggs and Ham?"};
        String [] [] answers = {{"George Bush", "George Washington", "John Locke"}, {"Taylor Swift", "Miley Cyrus", "Jack Daniels"}, {"Dr. Seuss", "Smokey", "JK Rowling"}};
        BasicAlgorithmConcurrent b = new BasicAlgorithmConcurrent(questions, answers);
        for(int i: b.getAnswers())
            System.out.print(i); */

     
     GameTime(args);
  //      String [][] game = Utilities.jsonToGame();
  //      TestRobot tester = new TestRobot(game, 300);
  //      tester.run();
   //  String [] stuff = {"Pink Floyd’s “Dark Side of the Moon” most famously syncs with what movie?", "The Empire Strikes Back", "Apollo 13", "The Wizard of Oz"};
   //  String [] stuff = {"Which of these Robin Williams characters is a real person?","Daniel Hillard", "Patch Adams", "T. S. Garp"};
    //   L.timeStamp = true;
     //  String [] stuff = {"Who was the first president of the united states?", "Jack Himilton", "Alex Johnson", "George Washington"};
     //  GameTime(stuff);
    }
    
    public static void GameTime(String [] args)
    {
       // L.blockAll();
       // L.unBlock(Logger.Channel.BASIC_ANSWER);
      //  L.unBlock(Logger.Channel.GOOGLE_USER_BASIC_FEATURED);
     //   L.unBlock(Logger.Channel.GOOGLE_USER_BASIC);
        scheduler.scheduleWithFixedDelay(new Runnable() {
            public void run() {
                L.print();
            }
        }, 0, 40, TimeUnit.MILLISECONDS);
        BasicAlgorithm b = new BasicAlgorithm(args, 1);
        b.getAnswer();
        L.print();
        scheduler.shutdown();
    }
      
  /*
    Some Utility functions
    */  
    public static void runRobot(String [] args) throws Exception
    {
        final String[] answers = {"A","B","C"};
        Question question = new Question(args);
        System.out.println(question.toString());
        if(!question.getFeatured().equals(""))
            System.out.println("Google Thinks: " + question.getFeatured());
        ArrayList<Double> basicAnswer = question.getBasicAnswer();
        System.out.println(Utilities.normalize(basicAnswer));
        int idx = Utilities.maxIndex(basicAnswer, true);
        if(idx > -1)
        {
            System.out.println("Answer:" + answers[idx]);
            System.out.println("__" + idx + "__");
        }
        //Start doing some advanced stuff down here. We will allow each answer choice to be a question and the choices will be the words of the question.
        //Then we will get the total hits 
        /*ArrayList<Question> results = new ArrayList<Question>();
        for(int i = 0; i < args.length - 1; i ++)
            results.add(new Question(args[1 + i], args[0].replaceAll("[^a-zA-Z ]", " ").toLowerCase().trim().split("\\s+")));
        for(Question q: results)    
            System.out.println(q.getTotalHits());*/
        
        
//This section is an attempt to deal with "which of these" questions in a specific way.
        
        if(args[0].toLowerCase().replace("?", "").contains("which"))
        {
            ArrayList<Double> results = question.WhichOfTheseSearch();
            System.out.println(Utilities.normalize(results));
            idx = Utilities.maxIndex(results, true);
            if(idx > -1)
            {
                System.out.println("WOT Answer: " + question.getAnswers().get(idx));
                 System.out.println("__" + idx + "__");
            }
        }
        
    }
    
    
    
    public static int getAnswer(String [] args, boolean printVector, boolean whichOfThese, boolean reverse, double WOTThresh,double RevThresh) throws Exception
    {
        int rtn;
        ArrayList<Double> results;
        final String[] answers = {"A","B","C"};
        Question question = new Question(args);
   //     System.out.println(question.toString());
        //if(!question.getFeatured().equals(""))
        //    System.out.println("Google Thinks: " + question.getFeatured());
        results = question.getBasicAnswer();
        rtn = Utilities.maxIndex(results, true);
        System.out.print("\n\t\tBasic: " + rtn);
        
        
//This section is an attempt to deal with "which of these" questions in a specific way.
        
        if(whichOfThese && rtn == -1 && args[0].toLowerCase().replace("?", "").contains("which"))
        {
           results = question.WhichOfTheseSearch(WOTThresh);
           rtn = Utilities.maxIndex(results, true);
           System.out.print("\n\t\tWOT: " + rtn);
        }
        if(printVector)
            System.out.println(results);
        
        //Start doing some advanced stuff down here. We will allow each answer choice to be a question and the choices will be the words of the question.
        //Then we will get the total hits 
        if(reverse && rtn == -1)
        {
            ArrayList<Double> hits = new ArrayList<Double>();
            ArrayList<Question> reverseResults = new ArrayList<Question>();
            for(int i = 0; i < args.length - 1; i ++)
                reverseResults.add(new Question(args[1 + i], args[0].replaceAll("[^a-zA-Z ]", " ").toLowerCase().trim().split("\\s+")));
            for(Question q: reverseResults)    
                hits.add((double)q.getTotalHits(RevThresh));
            rtn = Utilities.maxIndex(hits, true);
            System.out.print("\n\t\tReverse: " + rtn);
        }
        
        System.out.println("");
        
       return rtn;
        
    }
    
    public static void tester(boolean createClassification, String path, int delayPerQuestion) throws Exception
    {
        PrintWriter writerVectors = null;
        PrintWriter writerClasses = null;
        if(createClassification)
        {
            writerVectors = new PrintWriter(path + "\\_vectors", "UTF-8");
            writerClasses = new PrintWriter(path + "\\_classes", "UTF-8");
        }
        double correct = 0;
        // TODO code application logic here
        double inconclusive = 0;
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        JSONArray ja = (JSONArray) obj;
        
        
        for(int i = 0; i < ja.size(); i ++)
        {
            //This is the boiler plate json extraction code;
            JSONObject jo = (JSONObject)ja.get(i);

            JSONObject options = (JSONObject)jo.get("options");

            String question = (String)jo.get("question");

            String option1 = (String)options.get("1");

            String option2 = (String)options.get("2");

            String option3 = (String)options.get("3");
            
            long answer = (long)jo.get("answer");
            System.out.println("Question " + i);
            String[] input = {question, option1, option2, option3};
            int prediction = getAnswer(input, false,true, false,0,0);
            if(prediction == answer - 1)
            {
                System.out.println("Correct\n");
                correct++;
                Correct.add(question);
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("0");
                }
            }
            else if (prediction == -1)
            {
                System.out.println("IDK");
                IDK.add(question);
                inconclusive++;
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("1");
                }
            }
            else
            {
                System.out.println("Incorrect\n");
                incorrect.add(question);
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("2");
                }
            }
            if(i % 20 == 0)
                System.out.println(correct/(i+1));
            TimeUnit.MILLISECONDS.sleep((long)((Math.random()+.25) * delayPerQuestion));
        }
        System.out.print(correct/ja.size());
    }
    
    public static void tester(boolean createClassification, String path) throws Exception
    {
        PrintWriter writerVectors = null;
        PrintWriter writerClasses = null;
        if(createClassification)
        {
            writerVectors = new PrintWriter(path + "\\_vectors", "UTF-8");
            writerClasses = new PrintWriter(path + "\\_classes", "UTF-8");
        }
        double correct = 0;
        // TODO code application logic here
        double inconclusive = 0;
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        JSONArray ja = (JSONArray) obj;
        
        
        for(int i = 0; i < ja.size(); i ++)
        {
            //This is the boiler plate json extraction code;
            JSONObject jo = (JSONObject)ja.get(i);

            JSONObject options = (JSONObject)jo.get("options");

            String question = (String)jo.get("question");

            String option1 = (String)options.get("1");

            String option2 = (String)options.get("2");

            String option3 = (String)options.get("3");
            
            long answer = (long)jo.get("answer");
            System.out.println("Question " + i);
            String[] input = {question, option1, option2, option3};
            int prediction = getAnswer(input, false,true, false,0,0);
            if(prediction == answer - 1)
            {
                System.out.println("Correct\n");
                correct++;
                Correct.add(question);
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("0");
                }
            }
            else if (prediction == -1)
            {
                System.out.println("IDK");
                IDK.add(question);
                inconclusive++;
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("1");
                }
            }
            else
            {
                System.out.println("Incorrect\n");
                incorrect.add(question);
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("2");
                }
            }
            if(i % 20 == 0)
                System.out.println(correct/(i+1));
            TimeUnit.MILLISECONDS.sleep((long)((Math.random()+.25) * 2000));
        }
        System.out.print(correct/ja.size());
    }
    
    public static void tester(int delayPerQuestion) throws Exception
    {
        double correct = 0;
        // TODO code application logic here
        double inconclusive = 0;
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        JSONArray ja = (JSONArray) obj;
        
        
        for(int i = 0; i < ja.size(); i ++)
        {
            //This is the boiler plate json extraction code;
            JSONObject jo = (JSONObject)ja.get(i);

            JSONObject options = (JSONObject)jo.get("options");

            String question = (String)jo.get("question");

            String option1 = (String)options.get("1");

            String option2 = (String)options.get("2");

            String option3 = (String)options.get("3");
            
            long answer = (long)jo.get("answer");
            System.out.println("Question " + i);
            String[] input = {question, option1, option2, option3};
            int prediction = getAnswer(input, false, true,false,0,0);
            if(prediction == answer - 1)
            {
                System.out.println("Correct\n");
                correct++;
                Correct.add(question);
            }
            else if (prediction == -1)
            {
                System.out.println("IDK");
                IDK.add(question);
                inconclusive++;
            }
            else
            {
                System.out.println("Incorrect\n");
                incorrect.add(question);
            }
            if(i % 20 == 0)
                System.out.println(correct/(i+1));
            TimeUnit.MILLISECONDS.sleep((long)((Math.random()+.25) * delayPerQuestion));
        }
        System.out.print(correct/ja.size());
    }
    
    /*
        A more advanced type of function to fool google, the first ArrayList is to specify "every x questions" and the second
        is the delay that happens every x questions. So for instance the arrays [1,5,20] [2000, 10000, 20000] tell the program
        that it should wait 2 seconds between questions, 10 seconds between every block of 5, and 20 seconds between every block
        of 20 quetions.
    */
    public static void tester(double [] questionMod, double [] delay) throws Exception
    {
        double correct = 0;
        // TODO code application logic here
        double inconclusive = 0;
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        JSONArray ja = (JSONArray) obj;
        
        
        for(int i = 0; i < ja.size(); i ++)
        {
            //This is the boiler plate json extraction code;
            JSONObject jo = (JSONObject)ja.get(i);

            JSONObject options = (JSONObject)jo.get("options");

            String question = (String)jo.get("question");

            String option1 = (String)options.get("1");

            String option2 = (String)options.get("2");

            String option3 = (String)options.get("3");
            
            long answer = (long)jo.get("answer");
            System.out.println("Question " + i);
            String[] input = {question, option1, option2, option3};
            int prediction = getAnswer(input, false, true, false,0,0);
            if(prediction == answer - 1)
            {
                System.out.println("Correct\n");
                correct++;
                Correct.add(question);
            }
            else if (prediction == -1)
            {
                System.out.println("IDK");
                IDK.add(question);
                inconclusive++;
            }
            else
            {
                System.out.println("Incorrect\n");
                incorrect.add(question);
            }
            for(int j = 0; j < questionMod.length; j ++)
                if(i % questionMod[j] == 0)
                    TimeUnit.MILLISECONDS.sleep((long)((Math.random() + .25) * delay[j]));
            if(i % 20 == 0)
                System.out.println(correct/i);
        }
        System.out.print(correct/ja.size());
    }
    
    public static void tester(boolean createClassification, String path, double [] questionMod, double [] delay) throws Exception
    {
        PrintWriter writerVectors = null;
        PrintWriter writerClasses = null;
        if(createClassification)
        {
            writerVectors = new PrintWriter(path + "\\_vectors", "UTF-8");
            writerClasses = new PrintWriter(path + "\\_classes", "UTF-8");
        }
        double correct = 0;
        // TODO code application logic here
        double inconclusive = 0;
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        JSONArray ja = (JSONArray) obj;
        
        
        for(int i = 0; i < ja.size(); i ++)
        {
            //This is the boiler plate json extraction code;
            JSONObject jo = (JSONObject)ja.get(i);

            JSONObject options = (JSONObject)jo.get("options");

            String question = (String)jo.get("question");

            String option1 = (String)options.get("1");

            String option2 = (String)options.get("2");

            String option3 = (String)options.get("3");
            
            long answer = (long)jo.get("answer");
            System.out.println("Question " + i);
            String[] input = {question, option1, option2, option3};
            int prediction = getAnswer(input, false, true, false,0,0);
            if(prediction == answer - 1)
            {
                System.out.println("Correct\n");
                correct++;
                Correct.add(question);
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("0");
                }
            }
            else if (prediction == -1)
            {
                System.out.println("IDK");
                IDK.add(question);
                inconclusive++;
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("1");
                }
            }
            else
            {
                System.out.println("Incorrect\n");
                incorrect.add(question);
                if(createClassification)
                {
                    writerVectors.println(Utilities.questionToVector(question, true));
                    writerClasses.println("2");
                }
            }
            for(int j = 0; j < questionMod.length; j ++)
                if(i % questionMod[j] == 0)
                    TimeUnit.MILLISECONDS.sleep((long)((Math.random() + .25) * delay[j]));
            if(i % 20 == 0)
                System.out.print(correct/i);
        }
        System.out.print(correct/ja.size());
    }
    
    public static void simulateGame(int numQuestions) throws Exception
    {
        ArrayList<Integer> questionNumbers = Utilities.randomVector(numQuestions, 0, 800, true);
        double inconclusive = 0;
        double correct = 0;
        double notCorrect = 0;
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        JSONArray ja = (JSONArray) obj;
        for(int i = 0; i < questionNumbers.size(); i ++)
        {
            //This is the boiler plate json extraction code;
            JSONObject jo = (JSONObject)ja.get(questionNumbers.get(i));

            JSONObject options = (JSONObject)jo.get("options");

            String question = (String)jo.get("question");

            String option1 = (String)options.get("1");

            String option2 = (String)options.get("2");

            String option3 = (String)options.get("3");
            
            long answer = (long)jo.get("answer");
            System.out.println("Question " + (i + 1));
            String[] input = {question, option1, option2, option3};
            int prediction = getAnswer(input, true, true, false,0,0);
            System.out.println("Prediction:" + prediction);
            System.out.println("Answer:" + (answer - 1));
            if(prediction == answer - 1)
            {
                System.out.println("Correct\n");
                correct++;
                Correct.add(question);
            }
            else if (prediction == -1)
            {
                System.out.println("IDK");
                IDK.add(question);
                inconclusive++;
            }
            else
            {
                System.out.println("Incorrect\n");
                incorrect.add(question);
                notCorrect++;
            }
        }
            System.out.println("\n\n\n######################SUMMARY######################");
            System.out.println("Correct: " + correct);
            System.out.println("Incorrect: " + notCorrect);
            System.out.println("IDK: " + inconclusive);
            double winChance = Math.pow(ACCURACY, correct) * Math.pow(.33333333, inconclusive) * Math.pow((1-ACCURACY)/2, notCorrect);
            System.out.println("You would have needed " + (1/winChance) + " bots to win this one");
    }
}
