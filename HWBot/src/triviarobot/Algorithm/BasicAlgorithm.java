/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Algorithm;

import triviarobot.Logger;
import triviarobot.Processor.BasicProcessor;
import triviarobot.Rtn.BasicRtn;
import triviarobot.Search.BasicSearch;
import triviarobot.MiscTools.Utilities;
import static triviarobot.TriviaRobot.L;

/**
 *
 * @author Ol'
 */

public class BasicAlgorithm extends AlgorithmType{
    public int depth = 0;
    public BasicAlgorithm(String [] input)
    {
        super(input);
    }
    public BasicAlgorithm(String [] input, int depth)
    {
        super(input);
        this.depth = depth;
    }
    
    @Override
    public int getAnswer()
    {
        double [] scores = new double [answers.length];
        
        BasicSearch b = new BasicSearch(question, depth);
        BasicRtn br = b.Search();
        br.Clean();
        BasicProcessor bp = new BasicProcessor(br, answers);
        scores = Utilities.addArrays(bp.Process(), scores);
        if(!question.toLowerCase().contains(" not "))
        {
            int idx = 1 + Utilities.maxScoreIndex(scores);
            L.Log(Logger.Channel.BASIC_ANSWER, ""+idx);
            return idx;
        }
        else
        {
            int idx = 1 + Utilities.minScoreIndex(scores);
            L.Log(Logger.Channel.BASIC_ANSWER, ""+idx);
            return idx;
        }
    }
}
