/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Algorithm;

import java.util.ArrayList;

/**
 *
 * @author Ol'
 */
public abstract class AlgorithmType {
    String question;
    String [] answers;
    
    public AlgorithmType(String [] input)
    {
        this.answers = new String[input.length - 1];
        this.question = input[0].toLowerCase();
        for(int i = 1; i < input.length; i++)
            this.answers[i-1] = input[i];
    }
    public AlgorithmType(String question, String [] answers)
    {
        this.answers = new String[answers.length];
        this.question = question.toLowerCase();
        for(int i = 1; i < answers.length; i++)
            this.answers[i-1] = answers[i];
    }
    public AlgorithmType(ArrayList<String> input)
    {
        this.answers = new String[input.size() - 1];
        this.question = input.get(0);
        input.remove(0);
        for(int i = 0; i < input.size() - 1; i++)
            this.answers[i] = input.get(i);
    }
    public AlgorithmType(String question, ArrayList<String> answers)
    {
        this.answers = new String[answers.size()];
        this.question = question;
        for(int i = 0; i < answers.size(); i++)
            this.answers[i] = answers.get(i);
    }
    public abstract int getAnswer();
}
