/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Algorithm;

import java.util.ArrayList;

/**
 *
 * @author Ol'
 */
public abstract class ConcurrentAlgorithmType {
    final String [] questions;
    final String [][] answers;
    
    //Takes in an array of questions followed by their three answers
    public ConcurrentAlgorithmType(String [][] input)
    {
        this.questions = new String[input.length];
        this.answers = new String[input.length][3];
        for(int i = 0; i < input.length; i++)
            this.questions[i] = input[i][0];
        for(int i = 0; i < input.length; i++)
            System.arraycopy(input[i], 1, this.answers[i], 0, input[0].length - 1);
    }
    public ConcurrentAlgorithmType(String [] questions, String [][] answers)
    {
        this.questions = questions;
        this.answers = answers;
    }
    public ConcurrentAlgorithmType(ArrayList<ArrayList<String>> input)
    {
        this.questions = new String[input.size()];
        this.answers = new String[input.size()][3];
        for(int i = 0; i < input.size(); i++)
            this.questions[i] = input.get(i).get(0);
        for(int i = 0; i < input.size(); i++)
            for(int j = 1; j < input.get(0).size(); j++)
                this.answers[i][j] = input.get(i).get(j);
    }
    public ConcurrentAlgorithmType(String [] questions, ArrayList<ArrayList<String>> answers)
    {
        this.questions = questions;
        this.answers = new String[answers.size()][3];
        for(int i = 0; i < answers.size(); i++)
            for(int j = 1; j < answers.get(0).size(); j++)
                this.answers[i][j] = answers.get(i).get(j);
    }
    
    public abstract int [] getAnswers() throws Exception;
    
}
