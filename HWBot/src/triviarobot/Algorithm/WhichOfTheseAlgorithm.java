/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Algorithm;

import triviarobot.MiscTools.Utilities;
import triviarobot.Processor.WhichOfTheseProcessor;
import triviarobot.Rtn.WhichOfTheseRtn;
import triviarobot.Search.WhichOfTheseSearch;

/**
 *
 * @author Ol'
 */
public class WhichOfTheseAlgorithm extends AlgorithmType{
    
    double threshold = 0;
    public WhichOfTheseAlgorithm(String [] input)
    {
        super(input);
    }
    public WhichOfTheseAlgorithm(String [] input, double threshold)
    {
        super(input);
        this.threshold = threshold;
    }
    
    @Override
    public int getAnswer()
    {
        WhichOfTheseSearch w = new WhichOfTheseSearch(question, answers);
        WhichOfTheseRtn wr = w.Search();
        wr.Clean();
        WhichOfTheseProcessor wp = new WhichOfTheseProcessor(wr, answers, threshold);
        return  1 + Utilities.maxScoreIndex(wp.Process());
    }
}
