/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Algorithm;

import triviarobot.Processor.BasicProcessor;
import triviarobot.Rtn.BasicRtn;
import triviarobot.Search.BasicSearch;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import triviarobot.MiscTools.Utilities;
import triviarobot.MiscTools.WebUtilities;

/**
 *
 * @author Ol'
 */
public class BasicAlgorithmConcurrent extends ConcurrentAlgorithmType{
    String [] URLs;
    int depth = 0;
    public BasicAlgorithmConcurrent(String [][] input)
    {
        super(input);
    }
    public BasicAlgorithmConcurrent(String [][] input, int depth)
    {
        super(input);
        this.depth = depth;
    }
    
    public BasicAlgorithmConcurrent(String [][] input, String[] URLs)
    {
        super(input);
        this.URLs = URLs;
    }
    
    public BasicAlgorithmConcurrent(String [] questions, String [][] answers)
    {
        super(questions, answers);
    }
    
    public BasicAlgorithmConcurrent(ArrayList<ArrayList<String>> input)
    {
        super(input);
    }
    public  BasicAlgorithmConcurrent(String [] questions, ArrayList<ArrayList<String>> answers)
    {
        super(questions,answers);
    }
    
    @Override
    public int [] getAnswers() throws Exception
    {
        if(URLs == null)
        {
            String [] googURL = {WebUtilities.GOOGLE_URL};
            URLs = googURL;
        }
            int [] predictions = new int[questions.length];
            ArrayList<CompletableFuture <double []>> futures = new ArrayList<>();
            for(int i = 0; i < questions.length; i++)
            {
                final String [] ans = answers[i];
                BasicSearch b = new BasicSearch(questions[i], depth);
                CompletableFuture<double []> completableFuture 
                = CompletableFuture.supplyAsync(() -> b.Search())
                .thenCompose(s -> CompletableFuture.supplyAsync(() -> rtnTransition(s ,ans)))
                .thenCompose(s1 -> CompletableFuture.supplyAsync(() -> s1.Process()));
                futures.add(completableFuture);
            }
            for(int i = 0; i < futures.size(); i++)
            {
                double [] scores = new double[answers[0].length];
                scores = Utilities.addArrays((futures.get(i).get()), scores);
                if(!questions[i].toLowerCase().contains(" not "))
                    predictions[i] = 1 + Utilities.maxScoreIndex(scores);
                else
                    predictions[i] = 1 + Utilities.minScoreIndex(scores);
            }
            return predictions;
    }
    private BasicProcessor rtnTransition(BasicRtn rtn, final String[] choices)
    {
        rtn.Clean();
        return new BasicProcessor(rtn, choices);
    }
}
