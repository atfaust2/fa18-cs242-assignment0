/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.Algorithm;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import triviarobot.MiscTools.Utilities;
import triviarobot.MiscTools.WebUtilities;
import triviarobot.Processor.BasicProcessor;
import triviarobot.Processor.ProcessorType;
import triviarobot.Processor.WhichOfTheseProcessor;
import triviarobot.Rtn.BasicRtn;
import triviarobot.Rtn.WhichOfTheseRtn;
import triviarobot.Search.BasicSearch;
import triviarobot.Search.WhichOfTheseSearch;

/**
 *
 * @author Ol'
 */
public class WhichOfTheseAlgorithmConcurrent extends ConcurrentAlgorithmType{
    String [] URLs;
    double threshold = 0;
    public WhichOfTheseAlgorithmConcurrent(String [][] input)
    {
        super(input);
    }
    public WhichOfTheseAlgorithmConcurrent(String [][] input,double threshold)
    {
        super(input);
        this.threshold = threshold;
    }
    
    public WhichOfTheseAlgorithmConcurrent(String [] questions, String [][] answers)
    {
        super(questions, answers);
    }
    
    public WhichOfTheseAlgorithmConcurrent(ArrayList<ArrayList<String>> input)
    {
        super(input);
    }
    public  WhichOfTheseAlgorithmConcurrent(String [] questions, ArrayList<ArrayList<String>> answers)
    {
        super(questions,answers);
    }
    
    @Override
    public int [] getAnswers() throws Exception
    {
            int [] predictions = new int[questions.length];
            ArrayList<CompletableFuture<double []>> futures = new ArrayList<>();
            for(int i = 0; i < questions.length; i++)
            {
                final String [] ans = answers[i];
                WhichOfTheseSearch w = new WhichOfTheseSearch(questions[i], answers[i]);
                CompletableFuture<double []> completableFuture 
                = CompletableFuture.supplyAsync(() -> w.Search())
                .thenCompose(s -> CompletableFuture.supplyAsync(() -> rtnTransition(s, ans)))
                .thenCompose(s1 -> CompletableFuture.supplyAsync(() -> s1.Process()));
                futures.add(completableFuture);
                if(!questions[i].toLowerCase().contains(" not "))
                    predictions[i] = 1 + Utilities.maxScoreIndex((completableFuture.get()));
                else
                    predictions[i] = 1 + Utilities.minScoreIndex((completableFuture.get()));
            }
            return predictions;
    }
    
    private WhichOfTheseProcessor rtnTransition(WhichOfTheseRtn rtn, final String[] choices)
    {
        rtn.Clean();
        return new WhichOfTheseProcessor(rtn, choices);
    }

            
}
