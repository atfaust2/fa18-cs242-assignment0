/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.MiscTools;

/**
 *
 * @author Ol'
 */
public class WebUtilities {
    //This is the user agent string that we use to do all of the searching online
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) C"
                + "hrome/64.0.3282.186 Safari/537.36";
    
    
    //This is the URL to which any google search should be appedned.
    public static final String GOOGLE_URL = "https://google.com/search?q=";
    
    
    /*
        Returns the URL for googling a specific query
    */
    public static String GoogleQueryURL(String query){return GOOGLE_URL + query;}
}
