/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triviarobot.MiscTools;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.TreeSet;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Ol'
 */
public class Utilities {
    
    public static String [] topWords = {"of" ,"the","which" ,"what" ,"a" ,"is" ,"these" ,"in" ,"to" ,"for" ,"was","not","name" ,"by","an" ,"on" ,"first" ,"u.s."
            ,"as" ,"with" ,"has","from" ,"and" ,"most" ,"its" ,"did" ,"does" ,"known" ,"typically","you" ,"named" ,"used" ,"his" ,"are" ,
            "have" ,"famous" ,"at" ,"that" ,"word" ,"do" ,"two","who","be","film" ,"would" ,"after" ,"game" ,"state" ,"common" ,"type" ,
            "company" ,"made" ,"often" ,"likely" ,"“the" ,"song" ,"countries" ,"popular" ,"book" ,"when" ,"also" ,"one" ,"once" ,"same" ,"p"
            + "art" ,"their" ,"how" ,"title" ,"into" ,"according" ,"movie" ,"english" ,"where" ,"classic" ,"country" ,"about" ,"people" ,
            "city" ,"term" ,"president" ,"animal" ,"can" ,"official" ,"form" ,"traditionally" ,"author" ,"tv" ,"person" ,"before","comes",
            "use","work","called","featured","animals","world","played","olympic" ,"american" ,"video" ,"created" ,"home" ,
            "stand" ,"character" ,"contains" ,"original" ,"sports" ,"product" ,"her" ,"usually" ,"new" ,"legend" ,"features" 
            ,"king" ,"food" ,"won" ,"legendary" ,"places" ,"old" ,"written" ,"films" ,"&" ,"novel" ,"considered" ,"capital" ,
            "were" ,"had" ,"chain" ,"hit" ,"major" ,"been" ,"states" ,"band" ,"shares" ,"appear" ,"definition" ,"movies" ,
            "iconic" ,"college" ,"olympics" ,"system" ,"other" ,"singer" ,"all" ,"real" ,"show" ,"play" ,"phrase" ,"whom" ,
            "it" ,"up" ,"out" ,"without" ,"last" ,"house" ,"time" ,"means" ,"under " ,"member" ,"games" ,"item" ,"born" ,
            "kind" ,"win" ,"gets" ,"nickname" ,"set" ,"actor" ,"thanks" ,"appeared" ,"john" ,"brand" ,"north" ,"feature" 
            ,"get" ,"place" ,"lead" ,"only" ,"like" ,"characters" ,"got" ,"musical" ,"became" ,"credited" ,"found" ,"currency"
            ,"ingredient" ,"team" ,"fashion" ,"famously" ,"things" ,"playing" ,"players" ,"day" ,"group" ,"island" ,"court" ,
            "series" ,"commonly" ,"baseball" ,"host" ,"logo" ,"cities" ,"appears" ,"highest" ,"star" ,"during" ,"theater" ,
            "rhyme" ,"year" ,"music" ,"female" ,"family" ,"authors" ,"body" ,"annual" ,"dance" ,"letter" ,"came" ,"art" ,"now"
            ,"refer" ,"board" ,"words" ,"language" ,"over" ,"century" ,"st." ,"doing" ,"terms" ,"rock" ,"own" ,"line" ,"player"};
    public static boolean [] inclusionVector = {false, false, false, true, false, false, false, false, false, true, true, false, false, true, false, false, false, false, true, true, true, false, true, false, false, true, false, true, false, false, false, true, false, false, false, false, false, true, false, false, true, false, false, false, false, true, false, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, true, false, false, true, false, false, true, false, true, false, true, false, true, true, true, false, false, true, false, false, false, false, true, false, true, true, false, false, false, true, true, true, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, true, false, true, false, false, true, false, false, false, false, false, false, true, true, false, true, true, false, false, false, false, false, false, true, false, false, false, true, false, false, true, true, false, true, false, false, false, false, false, false, false, false, true, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, true, true, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, true, true, false, false, true, false, false, false, false, false, false, false, true, false, false, false, true};
   public static final String [] STOP_WORDS = {"a","about","above","after","again","against","all","am","an","and","any","are","aren't","as","at","be","because","been","before","being","below","between","both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't","has","hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself","his","how","how's","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's","its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only","or","other","ought","our","ours","ourselves","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that","that's","the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","through","to","too","under","until","up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while","who","who's","whom","why","why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves"};
  public static int count(final String in, final String s)
  {
      int count = 0;
      for(int i = 0; i <= in.length() - s.length(); i ++)
          if(in.substring(i, i + s.length()).equals(s))
              count++;
      return count;
  }
  
  public static ArrayList<Double> normalize(final ArrayList<Double> in)
  {
      ArrayList<Double> rtn = new ArrayList<>();
      int total = 0;
      for(Double d: in)
      {
          total += d;
      }
      for(int i = 0; i < in.size(); i++)
      {
          rtn.add(in.get(i)/total);
      }
      return rtn;
  }
   public static double[] normalize(final double[] in)
  {
      double[] rtn = new double[in.length];
      int total = 0;
      for(Double d: in)
      {
          total += d;
      }
      for(int i = 0; i < in.length; i++)
      {
          rtn[i] = in[i]/total;
      }
      return rtn;
  }
  public static int maxScoreIndex(final double [] arr)
  {
      boolean idk = true;
      int idx = 0;
      for(int i = 1; i < arr.length; i++)
      {
          if(arr[i] > arr[idx])
          {
              idx = i;
          }
          idk = idk && arr[i] == arr[i-1];
      }
      if(idk)
          return -1;
      return idx;
  }
  
    public static int maxScoreIndex(final int [] arr)
  {
      boolean idk = true;
      int idx = 0;
      for(int i = 1; i < arr.length; i++)
      {
          if(arr[i] > arr[idx])
          {
              idx = i;
          }
          idk = idk && arr[i] == arr[i-1];
      }
      if(idk)
          return -1;
      return idx;
  }
  
  //Fix 1 + error later
  public static int charIndexToWordIndex(String in, int charIndex)
  {
      return 1 + allIndicesOf(in.substring(0, charIndex).trim(), " ").size();
  }
  
  public static int minScoreIndex(final double [] arr)
  {
      boolean idk = true;
      int idx = 0;
      for(int i = 1; i < arr.length; i++)
      {
          if(arr[i] < arr[idx])
          {
              idx = i;
          }
          idk = idk && arr[i] == arr[i-1];
      }
      if(idk)
          return -1;
      return idx;
  }
   
    public static int minScoreIndex(final int [] arr)
  {
      boolean idk = true;
      int idx = 0;
      for(int i = 1; i < arr.length; i++)
      {
          if(arr[i] < arr[idx])
          {
              idx = i;
          }
          idk = idk && arr[i] == arr[i-1];
      }
      if(idk)
          return -1;
      return idx;
  }
  
  public static double [] addArrays(double [] in1, double [] in2)
  {
      double[] rtn = new double [in1.length];
      for(int i = 0; i < in1.length; i++)
          rtn[i] = in1[i] + in2[i];
      return rtn;
  }
  public static int maxIndex(final ArrayList<Double> in)
  {
      int maxIndex = 0;
      for(int i = 0; i < in.size(); i++)
      {
          if(in.get(maxIndex) < in.get(i))
              maxIndex = i;
      }
      return maxIndex;
  }
  
    public static int maxIndex(final int [] in)
  {
      int maxIndex = 0;
      for(int i = 0; i < in.length; i++)
      {
          if(in[maxIndex] < in[i])
              maxIndex = i;
      }
      return maxIndex;
  }
  
  public static int maxIndex(final ArrayList<Double> in, boolean rejectNan)
  {
      int maxIndex = 0;
      for(int i = 0; i < in.size(); i++)
      {
          if(in.get(maxIndex) < in.get(i))
              maxIndex = i;
      }
      if(rejectNan && in.get(maxIndex) == 0)
        return -1;
      return maxIndex;
  }
  
  public static ArrayList<Double> arrayMult(final ArrayList<Double> in1, final ArrayList<Double> in2)
  {
      ArrayList<Double> rtn = new ArrayList<>();
      for(int i = 0; i < in1.size(); i++)
      {
          rtn.add(in1.get(i) * in2.get(i));
      }
      return rtn;
  }
  
    public static ArrayList<Double> arrayMult(final ArrayList<Double> in1, double d)
  {
      ArrayList<Double> rtn = new ArrayList<>();
      for(int i = 0; i < in1.size(); i++)
      {
          rtn.add(in1.get(i) * d);
      }
      return rtn;
  }
  
  public static ArrayList<Double> arrayAdd(final ArrayList<Double> in1, final ArrayList<Double> in2)
  {
      ArrayList<Double> rtn = new ArrayList<>();
      for(int i = 0; i < in1.size(); i++)
      {
          rtn.add(in1.get(i) + in2.get(i));
      }
      return rtn;
  }
  
  public static int [] arrayAdd(int [] in1, int [] in2)
  {
      int [] rtn = new int [in1.length];
      for(int i = 0; i < in1.length; i++)
      {
          rtn[i] = in1[i] + in2[i];
      }
      return rtn;
  }
  
  public static ArrayList<Double> arrayAdd(final ArrayList<Double> in1, double d)
  {
      ArrayList<Double> rtn = new ArrayList<>();
      for(int i = 0; i < in1.size(); i++)
      {
          rtn.add(in1.get(i) + d);
      }
      return rtn;
  }
  public static ArrayList<Double> arraySub(final ArrayList<Double> in1, double d)
  {
      ArrayList<Double> rtn = new ArrayList<>();
      for(int i = 0; i < in1.size(); i++)
      {
          rtn.add(in1.get(i) - d);
      }
      return rtn;
  }
  public static double arraySum(final ArrayList<Double> in1)
  {
      double rtn = 0;
      for(Double d : in1)
          rtn += d;
      return rtn;
  }
  public static ArrayList<Double> arrayRailu(final ArrayList<Double> in1)
  {
      ArrayList<Double> rtn = new ArrayList<>();
      for(Double d : in1)
          rtn.add(Math.max(d, 0));
      return rtn;
  }
  public static ArrayList<Integer> allIndicesOf(final ArrayList<String> in,String s)
  {
      ArrayList<Integer> rtn = new ArrayList<Integer>();
      for(int i = 0; i < in.size(); i ++)
          if(in.get(i).equals(s))
              rtn.add(i);
      return rtn;
  }

  public static ArrayList<Integer> allIndicesOf(final String in,String s)
  {
      if(in.equals("") || s.equals(""))
          return new ArrayList<Integer>();
      
      ArrayList<Integer> rtn = new ArrayList<Integer>();
      for(int i = 0; i < in.length() - s.length(); i ++)
          if(in.substring(i,i + s.length()).equals(s))
              rtn.add(i);
      return rtn;
  }
  
  public static ArrayList<Integer> allIndicesOfWord(final String in,String s)
  {
      if(in.equals("") || s.equals(""))
          return new ArrayList<Integer>();
      ArrayList<Integer> rtn = new ArrayList<Integer>();
      for(int i = 0; i < in.length() - s.length() - 2; i ++)
          if(in.substring(i,2 + i + s.length()).equals(" " + s + " "))
              rtn.add(i);
      return rtn;
  }
  
  public static String getRadiusPadded(String s, int center, int radius)
  {
      String temp = s;
      for(int i = 0; i < radius; i++)
      {
          temp = " " + temp + " ";
      }
      center += radius;
      return temp.substring(center - radius,center + radius);
  }
  public static String getRadius(String s, int center, int radius)
  {
      return s.substring(Math.max(0,center - radius), Math.min(center + radius, s.length()));
  }
  
  public static String stringArrayToString(final ArrayList<String> in)
  {
      String rtn = "";
      for(String s: in)
          rtn += s + " ";
      return rtn;
  }
  
  public static String stringArrayToString(final String[]  in)
  {
      String rtn = "";
      for(String s: in)
          rtn += s + " ";
      return rtn;
  }
  /*
   questionToVector is for classification purposes. One version takes a list to build the vector from, a 1 if the question
  contains word i in the list and 0 otherwise, the other version just uses a custom list that I got by running a frequency analysis
  on the questions dataset.
  */
   public static ArrayList<Double> questionToVector(String question, boolean useLength)
  {
      ArrayList<Double> rtn = new ArrayList<Double>();
      for(String s: topWords)
      {
          if(question.contains(s))
              rtn.add(1.0);
          else
              rtn.add(0.0);
      }
      if(useLength)
      {
          rtn.add((double)question.length());
      }
      return rtn;
  }
   
   
  public static ArrayList<Double> questionToVector(String question, ArrayList<String> wordsOfInterest, boolean useLength)
  {
      ArrayList<Double> rtn = new ArrayList<Double>();
      for(String s: wordsOfInterest)
      {
          if(question.contains(s))
              rtn.add(1.0);
          else
              rtn.add(0.0);
      }
      if(useLength)
      {
          rtn.add((double)question.length());
      }
      return rtn;
  }
  
  public static TreeSet<String> boolVectorToArray(ArrayList<Boolean> vector)
  {
      TreeSet<String> rtn = new TreeSet<String>();
      for(int i = 0 ; i < vector.size(); i++)
      {
          if(vector.get(i))
              rtn.add(topWords[i]);
      }
      return rtn;
  }
  
    public static TreeSet<String> boolVectorToArray(boolean[] vector)
  {
      TreeSet<String> rtn = new TreeSet<String>();
      for(int i = 0 ; i < vector.length; i++)
      {
          if(vector[i])
              rtn.add(topWords[i]);
      }
      return rtn;
  }
  
  
  public static ArrayList<Integer> randomVector(int size, int lowerBound, int upperBound, boolean disableRepeats)
  {
      if(disableRepeats && upperBound - lowerBound + 1 - size == 0)
          return null;

          Random rand = new Random();
          ArrayList<Integer> rtn = new ArrayList<Integer>();
          HashSet<Integer> hs = new HashSet<Integer>();
          for(int i = 0; i < size; i ++)
          {
              int next = rand.nextInt(upperBound - lowerBound + 1) + lowerBound;
              if(disableRepeats)
              {
                if(!hs.contains(next))
                  rtn.add(rand.nextInt(upperBound - lowerBound + 1) + lowerBound);
                else
                    i--;
                hs.add(next);
              }
          }
          return rtn;
  }
  
  public static ArrayList<String> destroyCommonalities(ArrayList<String> vector)
  {
      ArrayList<String> rtn = new ArrayList<String>();
      for(String s: vector)
          rtn.add(s);
      while(true)
      {
          String first = getFirstWord(rtn.get(0));
          String last = getLastWord(rtn.get(0));
          boolean commonStart = true;
          boolean commonEnd = true;
          for(String s: rtn)
          {
              commonStart = commonStart && first.equals(getFirstWord(s));
              commonEnd = commonEnd && last.equals(getLastWord(s));
          }
          if((!commonStart && !commonEnd) || rtn.isEmpty() || rtn.get(0).equals(""))
          {
              return rtn;
          }
          if(commonStart)
          {
              for(int i = 0; i < rtn.size(); i ++)
              {
                  rtn.set(i,  removeFirstWord(rtn.get(i)));
              }
          }
          if(commonEnd)
          {
            for(int i = 0; i < rtn.size(); i ++)
            {
                rtn.set(i,  removeLastWord(rtn.get(i)));
            }
          }
      }
  }
  private static String removeLastWord(String s)
  {
      return s.substring(0, s.lastIndexOf(" "));
  }
  
  private static String removeFirstWord(String s)
  {
      return s.substring(s.indexOf(" ") + 1);
  }
    
  private static String getFirstWord(String s)
  {
      int idx = s.indexOf(" ");
      if(idx >= 0)
        return s.substring(0, s.indexOf(" "));
      return s;
  }
  private static String getLastWord(String s)
  {
      int idx = s.lastIndexOf(" ");
      if(idx >= 0)
        return s.substring(s.lastIndexOf(" ") + 1);
      return s;
  }
  
  public static ArrayList<Double> compileResults(ArrayList<Boolean[]> geneListList)
  {
      ArrayList<Double> rtn = new ArrayList<Double>();
      for(int i = 0; i < geneListList.get(0).length; i++)
          rtn.add(0.0);
      int count = 0;
      for(Boolean[] geneList: geneListList)
      {
          for(Boolean g : geneList)
          {
              if(g)
                  rtn.set(count, rtn.get(count) + 1);
              count ++;
          }
          count = 0;
      }
      return rtn;
  }
  
  public static String [][] jsonToGame(int num)
  {
      JSONArray ja = null;
      try
      {
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        ja = (JSONArray) obj;
      }
      catch(Exception e)
      {
          System.out.println("Could not find file, use method version that takes in a file path");
          return null;
      }
      if(num > ja.size())
          System.err.println("We do not have enough questions for a game that large");
       
      String [][] game = new String[ja.size()][5];

      for(int i = 0; i < num; i ++)
      {
        //This is the boiler plate json extraction code;
        JSONObject jo = (JSONObject)ja.get(i);

        JSONObject options = (JSONObject)jo.get("options");

        String question = (String)jo.get("question");

        String option1 = (String)options.get("1");

        String option2 = (String)options.get("2");

        String option3 = (String)options.get("3");

        String answer = "" + jo.get("answer");
        
        String [] question_inst = {question, option1, option2, option3, answer};
        game[i] = question_inst;
      }
      return game;
  }
  
    public static String [][] jsonToGame()
  {
      JSONArray ja = null;
      try
      {
        Object obj = new JSONParser().parse(new FileReader("C:\\Users\\Ol'\\Desktop\\TriviaQuestions.json"));
        
        ja = (JSONArray) obj;
      }
      catch(Exception e)
      {
          System.out.println("Could not find file, use method version that takes in a file path");
          return null;
      }
       
      String [][] game = new String[ja.size()][5];

      for(int i = 0; i < ja.size(); i ++)
      {
        //This is the boiler plate json extraction code;
        JSONObject jo = (JSONObject)ja.get(i);

        JSONObject options = (JSONObject)jo.get("options");

        String question = (String)jo.get("question");

        String option1 = (String)options.get("1");

        String option2 = (String)options.get("2");

        String option3 = (String)options.get("3");

        String answer = "" + jo.get("answer");
        
        
        String [] question_inst = {question, option1, option2, option3, answer};
        game[i] = question_inst;
      }
      return game;
  }
  
  public static String [][] jsonToGame(String path)
  {
      JSONArray ja = null;
      try
      {
        Object obj = new JSONParser().parse(new FileReader(path));
        
        ja = (JSONArray) obj;
      }
      catch(Exception e)
      {
          System.out.println("Could not find file, use method version that takes in a file path");
          return null;
      }
       
      String [][] game = new String[ja.size()][5];

      for(int i = 0; i < ja.size(); i ++)
      {
        //This is the boiler plate json extraction code;
        JSONObject jo = (JSONObject)ja.get(i);

        JSONObject options = (JSONObject)jo.get("options");

        String question = (String)jo.get("question");

        String option1 = (String)options.get("1");

        String option2 = (String)options.get("2");

        String option3 = (String)options.get("3");

        String answer = "" + jo.get("answer");
        
        
        String [] question_inst = {question, option1, option2, option3, answer};
        game[i] = question_inst;
      }
      return game;
  }
  
  
  public static String [][] jsonToGame(int num, String path)
  {
      JSONArray ja = null;
      try
      {
        Object obj = new JSONParser().parse(new FileReader(path));
        
        ja = (JSONArray) obj;
      }
      catch(Exception e)
      {
          System.err.println("Could not find file, use method version that takes in a file path");
          return null;
      }
      if(num < ja.size())
          System.err.print("We do not have enough questions for a game that large");
      
      String [][] game = new String[ja.size()][5];
      
      for(int i = 0; i < num; i ++)
      {
        //This is the boiler plate json extraction code;
        JSONObject jo = (JSONObject)ja.get(i);

        JSONObject options = (JSONObject)jo.get("options");

        String question = (String)jo.get("question");

        String option1 = (String)options.get("1");

        String option2 = (String)options.get("2");

        String option3 = (String)options.get("3");

        String answer = "" + jo.get("answer");
        
        String [] question_inst = {question, option1, option2, option3, answer};
        game[i] = question_inst;
      }
      return game;
  }
  
  public static int [] arrayFreq(String in, String [] wordList)
  {      
      String temp = in;
      int [] rtn = new int[wordList.length];
      int maxLen = 0;
      for(String s: wordList)
      {
          if (s.length() > maxLen)
              maxLen = s.length();
      }
      for(int i = 0; i < maxLen + 1; i++)
          temp += " ";
      for(int i = 0; i < in.length(); i ++)
      {
          int j = 0;
          for(String s: wordList)
          {
            if(temp.substring(i,i + s.length()).equals(s))
                rtn[j]++;
            j++;
          }
      }
      return rtn;
  }

    public static String StringArrayToString(ArrayList<String> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
}
